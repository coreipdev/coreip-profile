package com.group.coreip.models;

public class CacheDataInfo {
	private String KEY;
	private String VALUE;
	private String NAME;
	private String DEFAULTVAL;
	private String TYPE;
	private String COMMENTED;

	
	public String getDEFAULTVAL() {
		return DEFAULTVAL;
	}


	public void setDEFAULTVAL(String dEFAULTVAL) {
		DEFAULTVAL = dEFAULTVAL;
	}


	public String getCOMMENTED() {
		return COMMENTED;
	}


	public void setCOMMENTED(String cOMMENTED) {
		COMMENTED = cOMMENTED;
	}


	public CacheDataInfo(String key, String value, String name, String defaultVal, String commented) {
		// TODO Auto-generated constructor stub
		this.KEY = key;
		this.VALUE = value;
		this.NAME = name;
		this.DEFAULTVAL = defaultVal;
		this.COMMENTED = commented;
	}

	
	public String getTYPE() {
		return TYPE;
	}


	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}


	public CacheDataInfo() {
		super();
	}

	
	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getKEY() {
		return KEY;
	}

	public void setKEY(String kEY) {
		KEY = kEY;
	}

	public String getVALUE() {
		return VALUE;
	}

	public void setVALUE(String vALUE) {
		VALUE = vALUE;
	}
}
