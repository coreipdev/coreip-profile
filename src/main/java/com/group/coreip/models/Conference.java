package com.group.coreip.models;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Conference
{
	String confEnable;
	String exSunday;
	String confTime;
	Date scheduleTime;
	String confOwner;
	String ownerPwd;
	String confRoom;
	String conferenceID;
	String frequency;			//daily, weekly, monthly, yearly.
	
	public Conference()
	{
		super();
		this.frequency = "yearly";
	}
	
	public String getConfOwner() {
		return confOwner;
	}
	public void setConfOwner(String confOwner) {
		this.confOwner = confOwner;
	}
	public String getOwnerPwd() {
		return ownerPwd;
	}
	public void setOwnerPwd(String ownerPwd) {
		this.ownerPwd = ownerPwd;
	}
	public Date getScheduleTime() {
		return scheduleTime;
	}
	public void setScheduleTime(Date scheduleTime) {
		this.scheduleTime = scheduleTime;
	}
	
	public String getConferenceID()
	{
		return conferenceID;
	}
	public void setConferenceID(String conferenceID) {
		this.conferenceID = conferenceID;
	}
	
	public String getConfRoom() {
		return confRoom;
	}
	public void setConfRoom(String confRoom) {
		this.confRoom = confRoom;
	}
	 	
	public String isExSunday() {
		return exSunday;
	}
	public void setExSunday(String exSunday) {
		this.exSunday = exSunday;
	}
	public String getConfEnable() {
		return confEnable;
	}
	public void setConfEnable(String confEnable) {
		this.confEnable = confEnable;
	}

	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getConfTime() {
		return confTime;
	}
	public void setConfTime(String confTime) {
		this.confTime = confTime;
	}	
}
