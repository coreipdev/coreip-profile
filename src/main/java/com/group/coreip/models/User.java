package com.group.coreip.models;

import java.util.Date;


public class User
{
	private String userID;
	private String range;
	private String pin;
	private String extensionNo;
	private String sipPassword;
	private String callerId;
	private String firstName;
	private String lastName;
	private String host;
	private String disallow;
	private String allow;
	private String context;
	private String hassip;
	private String callwaiting;
	private String nat;
	private String transfer;
	private String ipVersion;
	private String qualify;

	private Date creationDate;
	

	//SipXecs Data
	private String sipxId;
	private String groups;
	private String contacts;
	private String expires;
	private String identity;
	private String isRegistered;
	private boolean superadminAccess;
	private boolean localAuthentication;
	private boolean changePINfromIVR;
	private boolean configurePersonalAutoAttendant;
	private boolean configureMusiconHold;
	private boolean configureGroupMusiconHold;
	private boolean subscribetoPresence;
	private boolean dialing900;
	private boolean attendantDirectory;
	private boolean internationalDialing;
	private boolean localDialing;
	private boolean longDistanceDialing;
	private boolean mobileDialing;
	private boolean tollFree;
	private boolean voiceMail;
	private boolean recordSystemPrompts;
	private boolean configPA;
	private boolean initialInternationalDialing;
	private boolean initialLocalDialing;
	private boolean initialLongDistanceDialing;
	private boolean initialMobileDialing;

	

	public User() 
	{
		super();
		this.extensionNo = "Dummy";
		this.userID = "";
		this.host = "dynamic";
		this.hassip = "yes";
		this.callwaiting = "yes";
		this.disallow = "all";
		this.allow = "ulaw,h263,h263p,h264";
		this.qualify = "yes";
		this.context = "local";
		this.firstName = "First Name";
		this.lastName = "Last Name";
		this.sipPassword = "1234";
		this.pin = "12345678";
		this.transfer = "yes";
		this.nat = "no";
		this.callerId = "91124xxxxxxx";
		this.configPA = false;
	}

	public User(String userID, String range, String pin, String extensionNo, String sipPassword, String callerId,
			String firstName, String lastName, String host, String disallow, String allow, String context,
			String hassip, String callwaiting, String nat, String transfer, String ipVersion, String qualify,
			Date creationDate, String sipxId, String groups, String contacts, String expires, String identity,
			String isRegistered, boolean superadminAccess, boolean localAuthentication, boolean changePINfromIVR,
			boolean configurePersonalAutoAttendant, boolean configureMusiconHold, boolean configureGroupMusiconHold,
			boolean subscribetoPresence, boolean dialing900, boolean attendantDirectory, boolean internationalDialing,
			boolean localDialing, boolean longDistanceDialing, boolean mobileDialing, boolean tollFree,
			boolean voiceMail, boolean recordSystemPrompts) {
		super();
		this.userID = userID;
		this.range = range;
		this.pin = pin;
		this.extensionNo = extensionNo;
		this.sipPassword = sipPassword;
		this.callerId = callerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.host = host;
		this.disallow = disallow;
		this.allow = allow;
		this.context = context;
		this.hassip = hassip;
		this.callwaiting = callwaiting;
		this.nat = nat;
		this.transfer = transfer;
		this.ipVersion = ipVersion;
		this.qualify = qualify;
		this.creationDate = creationDate;
		this.sipxId = sipxId;
		this.groups = groups;
		this.contacts = contacts;
		this.expires = expires;
		this.identity = identity;
		this.isRegistered = isRegistered;
		this.superadminAccess = superadminAccess;
		this.localAuthentication = localAuthentication;
		this.changePINfromIVR = changePINfromIVR;
		this.configurePersonalAutoAttendant = configurePersonalAutoAttendant;
		this.configureMusiconHold = configureMusiconHold;
		this.configureGroupMusiconHold = configureGroupMusiconHold;
		this.subscribetoPresence = subscribetoPresence;
		this.dialing900 = dialing900;
		this.attendantDirectory = attendantDirectory;
		this.internationalDialing = internationalDialing;
		this.localDialing = localDialing;
		this.longDistanceDialing = longDistanceDialing;
		this.mobileDialing = mobileDialing;
		this.tollFree = tollFree;
		this.voiceMail = voiceMail;
		this.recordSystemPrompts = recordSystemPrompts;
		
	}


	public boolean isConfigPA() {
		return configPA;
	}

	public void setConfigPA(boolean configPA) {
		this.configPA = configPA;
	}
	
	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public String getQualify() {
		return qualify;
	}

	public void setQualify(String qualify) {
		this.qualify = qualify;
	}
	
	public String getContacts() {
		return contacts;
	}

	public void setContacts(String contacts) {
		this.contacts = contacts;
	}

	public String getExpires() {
		return expires;
	}

	public void setExpires(String expires) {
		this.expires = expires;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getIpVersion() {
		return ipVersion;
	}

	public void setIpVersion(String ipVersion) {
		this.ipVersion = ipVersion;
	}
	
	public String getIsRegistered() {
		return isRegistered;
	}

	public void setIsRegistered(String isRegistered) {
		this.isRegistered = isRegistered;
	}

	public String getExtensionNo() {
		return extensionNo;
	}

	public void setExtensionNo(String extensionNo) {
		this.extensionNo = extensionNo;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getSipPassword() {
		return sipPassword;
	}

	public void setSipPassword(String sipPassword) {
		this.sipPassword = sipPassword;
	}
	
	public String getCallerId() {
		return callerId;
	}

	public void setCallerId(String callerId) {
		this.callerId = callerId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getSipxId() {
		return sipxId;
	}

	public void setSipxId(String sipxId) {
		this.sipxId = sipxId;
	}

	public String getGroups() {
		return groups;
	}

	public void setGroups(String groups) {
		this.groups = groups;
	}

	public boolean isSuperadminAccess() {
		return superadminAccess;
	}

	public void setSuperadminAccess(boolean superadminAccess) {
		this.superadminAccess = superadminAccess;
	}

	public boolean isLocalAuthentication() {
		return localAuthentication;
	}

	public void setLocalAuthentication(boolean localAuthentication) {
		this.localAuthentication = localAuthentication;
	}

	public boolean isChangePINfromIVR() {
		return changePINfromIVR;
	}

	public void setChangePINfromIVR(boolean changePINfromIVR) {
		this.changePINfromIVR = changePINfromIVR;
	}

	public boolean isConfigurePersonalAutoAttendant() {
		return configurePersonalAutoAttendant;
	}

	public void setConfigurePersonalAutoAttendant(boolean configurePersonalAutoAttendant) {
		this.configurePersonalAutoAttendant = configurePersonalAutoAttendant;
	}

	public boolean isConfigureMusiconHold() {
		return configureMusiconHold;
	}

	public void setConfigureMusiconHold(boolean configureMusiconHold) {
		this.configureMusiconHold = configureMusiconHold;
	}

	public boolean isConfigureGroupMusiconHold() {
		return configureGroupMusiconHold;
	}

	public void setConfigureGroupMusiconHold(boolean configureGroupMusiconHold) {
		this.configureGroupMusiconHold = configureGroupMusiconHold;
	}

	public boolean isSubscribetoPresence() {
		return subscribetoPresence;
	}

	public void setSubscribetoPresence(boolean subscribetoPresence) {
		this.subscribetoPresence = subscribetoPresence;
	}

	public boolean isDialing900() {
		return dialing900;
	}

	public void setDialing900(boolean dialing900) {
		this.dialing900 = dialing900;
	}

	public boolean isAttendantDirectory() {
		return attendantDirectory;
	}

	public void setAttendantDirectory(boolean attendantDirectory) {
		this.attendantDirectory = attendantDirectory;
	}

	public boolean isInternationalDialing() {
		return internationalDialing;
	}

	public void setInternationalDialing(boolean internationalDialing) {
		this.internationalDialing = internationalDialing;
	}

	public boolean isLocalDialing() {
		return localDialing;
	}

	public void setLocalDialing(boolean localDialing) {
		this.localDialing = localDialing;
	}

	public boolean isLongDistanceDialing() {
		return longDistanceDialing;
	}

	public void setLongDistanceDialing(boolean longDistanceDialing) {
		this.longDistanceDialing = longDistanceDialing;
	}

	public boolean isMobileDialing() {
		return mobileDialing;
	}

	public void setMobileDialing(boolean mobileDialing) {
		this.mobileDialing = mobileDialing;
	}

	public boolean isTollFree() {
		return tollFree;
	}

	public void setTollFree(boolean tollFree) {
		this.tollFree = tollFree;
	}

	public boolean isVoiceMail() {
		return voiceMail;
	}

	public void setVoiceMail(boolean voiceMail) {
		this.voiceMail = voiceMail;
	}

	public boolean isRecordSystemPrompts() {
		return recordSystemPrompts;
	}

	public void setRecordSystemPrompts(boolean recordSystemPrompts) {
		this.recordSystemPrompts = recordSystemPrompts;
	}

	public String getUserID() {
		return userID;
	}
	
	public void setUserID(String userID) {
		this.userID = userID;
	}
		
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getHost() {
		return host;
	}
	
	public void setHost(String host) {
		this.host = host;
	}
	
	public String getDisallow() {
		return disallow;
	}
	
	public void setDisallow(String disallow) {
		this.disallow = disallow;
	}
	
	public String getAllow() {
		return allow;
	}
	
	public void setAllow(String allow) {
		this.allow = allow;
	}
	
	public String getContext() {
		return context;
	}
	
	public void setContext(String context) {
		this.context = context;
	}
	
	public String getHassip() {
		return hassip;
	}
	
	public void setHassip(String hassip) {
		this.hassip = hassip;
	}
	
	public String getCallwaiting() {
		return callwaiting;
	}
	
	public void setCallwaiting(String callwaiting) {
		this.callwaiting = callwaiting;
	}
	
	public String getNat() {
		return nat;
	}
	
	public void setNat(String nat) {
		this.nat = nat;
	}
	
	public String getTransfer() {
		return transfer;
	}
	
	public void setTransfer(String transfer) {
		this.transfer = transfer;
	}

	public boolean isInitialInternationalDialing() {
		return initialInternationalDialing;
	}

	public void setInitialInternationalDialing(boolean initialInternationalDialing) {
		this.initialInternationalDialing = initialInternationalDialing;
	}

	public boolean isInitialLocalDialing() {
		return initialLocalDialing;
	}

	public void setInitialLocalDialing(boolean initialLocalDialing) {
		this.initialLocalDialing = initialLocalDialing;
	}

	public boolean isInitialLongDistanceDialing() {
		return initialLongDistanceDialing;
	}

	public void setInitialLongDistanceDialing(boolean initialLongDistanceDialing) {
		this.initialLongDistanceDialing = initialLongDistanceDialing;
	}

	public boolean isInitialMobileDialing() {
		return initialMobileDialing;
	}

	public void setInitialMobileDialing(boolean initialMobileDialing) {
		this.initialMobileDialing = initialMobileDialing;
	}
	
}
