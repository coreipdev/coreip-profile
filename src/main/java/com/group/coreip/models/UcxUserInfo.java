package com.group.coreip.models;

public class UcxUserInfo {
	private String username;
	private String user_id;
	private String fName;
	private String lName;
	private String pintoken;
	private String sipPasswd;
	private String groups;
	private String aliases;
	private String extension;
	private String bal;
	
	

	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getBal() {
		return bal;
	}
	public void setBal(String bal) {
		this.bal = bal;
	}
	public String getGroups() {
		return groups;
	}
	public void setGroups(String groups) {
		this.groups = groups;
	}
	public String getAliases() {
		return aliases;
	}
	public void setAliases(String aliases) {
		this.aliases = aliases;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getPintoken() {
		return pintoken;
	}
	public void setPintoken(String pintoken) {
		this.pintoken = pintoken;
	}
	public String getSipPasswd() {
		return sipPasswd;
	}
	public void setSipPasswd(String sipPasswd) {
		this.sipPasswd = sipPasswd;
	}

}
