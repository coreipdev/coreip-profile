package com.group.coreip.models;

import java.util.Date;

import org.springframework.data.mongodb.core.index.Indexed;

public class SipxUserProfile {

	private String m_userid;
	private String m_userName;
	private String m_authAccountName;
	private String m_firstName;
	private String m_lastName;

	private String m_jobTitle;
	private String m_jobDept;
	private String m_companyName;
	private String m_assistantName;
	private String m_location;

	// private Address m_homeAddress = new Address();
	// private Address m_officeAddress = new Address();
	// private Address m_branchAddress = new Address();
	private String m_cellPhoneNumber;
	private String m_homePhoneNumber;
	private String m_assistantPhoneNumber;
	private String m_faxNumber;
	private String m_didNumber;
	private String m_imId;
	private String m_imDisplayName;
	private String m_alternateImId;
	private String m_emailAddress;
	private String m_alternateEmailAddress;
	private String m_emailAddressAliases;
	private boolean m_useBranchAddress;
	private String m_branchName;
	private String m_manager;
	private String m_salutation;
	private String m_employeeId;
	private String m_twiterName;
	private String m_linkedinName;
	private String m_facebookName;
	private String m_xingName;
	private long m_timestamp;

	private String m_avatar;
	private String m_extAvatar;
	private boolean m_useExtAvatar;

	private boolean m_enabled = true;
	private boolean m_ldapManaged;
	private Date m_lastImportedDate;
	private Date m_disabledDate;

	private Date m_extAvatarSyncDate;
	
	@Indexed
	private String m_custom1;
	@Indexed
	private String m_custom2;
	@Indexed
	private String m_custom3;

	public String getM_userid() {
		return m_userid;
	}

	public void setM_userid(String m_userid) {
		this.m_userid = m_userid;
	}

	public String getM_userName() {
		return m_userName;
	}

	public void setM_userName(String m_userName) {
		this.m_userName = m_userName;
	}

	public String getM_authAccountName() {
		return m_authAccountName;
	}

	public void setM_authAccountName(String m_authAccountName) {
		this.m_authAccountName = m_authAccountName;
	}

	public String getM_firstName() {
		return m_firstName;
	}

	public void setM_firstName(String m_firstName) {
		this.m_firstName = m_firstName;
	}

	public String getM_lastName() {
		return m_lastName;
	}

	public void setM_lastName(String m_lastName) {
		this.m_lastName = m_lastName;
	}

	public String getM_jobTitle() {
		return m_jobTitle;
	}

	public void setM_jobTitle(String m_jobTitle) {
		this.m_jobTitle = m_jobTitle;
	}

	public String getM_jobDept() {
		return m_jobDept;
	}

	public void setM_jobDept(String m_jobDept) {
		this.m_jobDept = m_jobDept;
	}

	public String getM_companyName() {
		return m_companyName;
	}

	public void setM_companyName(String m_companyName) {
		this.m_companyName = m_companyName;
	}

	public String getM_assistantName() {
		return m_assistantName;
	}

	public void setM_assistantName(String m_assistantName) {
		this.m_assistantName = m_assistantName;
	}

	public String getM_location() {
		return m_location;
	}

	public void setM_location(String m_location) {
		this.m_location = m_location;
	}

	public String getM_cellPhoneNumber() {
		return m_cellPhoneNumber;
	}

	public void setM_cellPhoneNumber(String m_cellPhoneNumber) {
		this.m_cellPhoneNumber = m_cellPhoneNumber;
	}

	public String getM_homePhoneNumber() {
		return m_homePhoneNumber;
	}

	public void setM_homePhoneNumber(String m_homePhoneNumber) {
		this.m_homePhoneNumber = m_homePhoneNumber;
	}

	public String getM_assistantPhoneNumber() {
		return m_assistantPhoneNumber;
	}

	public void setM_assistantPhoneNumber(String m_assistantPhoneNumber) {
		this.m_assistantPhoneNumber = m_assistantPhoneNumber;
	}

	public String getM_faxNumber() {
		return m_faxNumber;
	}

	public void setM_faxNumber(String m_faxNumber) {
		this.m_faxNumber = m_faxNumber;
	}

	public String getM_didNumber() {
		return m_didNumber;
	}

	public void setM_didNumber(String m_didNumber) {
		this.m_didNumber = m_didNumber;
	}

	public String getM_imId() {
		return m_imId;
	}

	public void setM_imId(String m_imId) {
		this.m_imId = m_imId;
	}

	public String getM_imDisplayName() {
		return m_imDisplayName;
	}

	public void setM_imDisplayName(String m_imDisplayName) {
		this.m_imDisplayName = m_imDisplayName;
	}

	public String getM_alternateImId() {
		return m_alternateImId;
	}

	public void setM_alternateImId(String m_alternateImId) {
		this.m_alternateImId = m_alternateImId;
	}

	public String getM_emailAddress() {
		return m_emailAddress;
	}

	public void setM_emailAddress(String m_emailAddress) {
		this.m_emailAddress = m_emailAddress;
	}

	public String getM_alternateEmailAddress() {
		return m_alternateEmailAddress;
	}

	public void setM_alternateEmailAddress(String m_alternateEmailAddress) {
		this.m_alternateEmailAddress = m_alternateEmailAddress;
	}

	public String getM_emailAddressAliases() {
		return m_emailAddressAliases;
	}

	public void setM_emailAddressAliases(String m_emailAddressAliases) {
		this.m_emailAddressAliases = m_emailAddressAliases;
	}

	public boolean isM_useBranchAddress() {
		return m_useBranchAddress;
	}

	public void setM_useBranchAddress(boolean m_useBranchAddress) {
		this.m_useBranchAddress = m_useBranchAddress;
	}

	public String getM_branchName() {
		return m_branchName;
	}

	public void setM_branchName(String m_branchName) {
		this.m_branchName = m_branchName;
	}

	public String getM_manager() {
		return m_manager;
	}

	public void setM_manager(String m_manager) {
		this.m_manager = m_manager;
	}

	public String getM_salutation() {
		return m_salutation;
	}

	public void setM_salutation(String m_salutation) {
		this.m_salutation = m_salutation;
	}

	public String getM_employeeId() {
		return m_employeeId;
	}

	public void setM_employeeId(String m_employeeId) {
		this.m_employeeId = m_employeeId;
	}

	public String getM_twiterName() {
		return m_twiterName;
	}

	public void setM_twiterName(String m_twiterName) {
		this.m_twiterName = m_twiterName;
	}

	public String getM_linkedinName() {
		return m_linkedinName;
	}

	public void setM_linkedinName(String m_linkedinName) {
		this.m_linkedinName = m_linkedinName;
	}

	public String getM_facebookName() {
		return m_facebookName;
	}

	public void setM_facebookName(String m_facebookName) {
		this.m_facebookName = m_facebookName;
	}

	public String getM_xingName() {
		return m_xingName;
	}

	public void setM_xingName(String m_xingName) {
		this.m_xingName = m_xingName;
	}

	public long getM_timestamp() {
		return m_timestamp;
	}

	public void setM_timestamp(long m_timestamp) {
		this.m_timestamp = m_timestamp;
	}

	public String getM_avatar() {
		return m_avatar;
	}

	public void setM_avatar(String m_avatar) {
		this.m_avatar = m_avatar;
	}

	public String getM_extAvatar() {
		return m_extAvatar;
	}

	public void setM_extAvatar(String m_extAvatar) {
		this.m_extAvatar = m_extAvatar;
	}

	public boolean isM_useExtAvatar() {
		return m_useExtAvatar;
	}

	public void setM_useExtAvatar(boolean m_useExtAvatar) {
		this.m_useExtAvatar = m_useExtAvatar;
	}

	public boolean isM_enabled() {
		return m_enabled;
	}

	public void setM_enabled(boolean m_enabled) {
		this.m_enabled = m_enabled;
	}

	public boolean isM_ldapManaged() {
		return m_ldapManaged;
	}

	public void setM_ldapManaged(boolean m_ldapManaged) {
		this.m_ldapManaged = m_ldapManaged;
	}

	public Date getM_lastImportedDate() {
		return m_lastImportedDate;
	}

	public void setM_lastImportedDate(Date m_lastImportedDate) {
		this.m_lastImportedDate = m_lastImportedDate;
	}

	public Date getM_disabledDate() {
		return m_disabledDate;
	}

	public void setM_disabledDate(Date m_disabledDate) {
		this.m_disabledDate = m_disabledDate;
	}

	public Date getM_extAvatarSyncDate() {
		return m_extAvatarSyncDate;
	}

	public void setM_extAvatarSyncDate(Date m_extAvatarSyncDate) {
		this.m_extAvatarSyncDate = m_extAvatarSyncDate;
	}

	public String getM_custom1() {
		return m_custom1;
	}

	public void setM_custom1(String m_custom1) {
		this.m_custom1 = m_custom1;
	}

	public String getM_custom2() {
		return m_custom2;
	}

	public void setM_custom2(String m_custom2) {
		this.m_custom2 = m_custom2;
	}

	public String getM_custom3() {
		return m_custom3;
	}

	public void setM_custom3(String m_custom3) {
		this.m_custom3 = m_custom3;
	}
}
