package com.group.coreip.models;

public class SipxUserInfo {
	
	private String username;
	private String user_id;
	private String fName;
	private String lName;
	private String pintoken;
	private String sipPasswd;
	private String valueStorageId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getPintoken() {
		return pintoken;
	}

	public void setPintoken(String pintoken) {
		this.pintoken = pintoken;
	}

	public String getSipPasswd() {
		return sipPasswd;
	}

	public void setSipPasswd(String sipPasswd) {
		this.sipPasswd = sipPasswd;
	}

	public String getValueStorageId() {
		return valueStorageId;
	}

	public void setValueStorageId(String valueStorageId) {
		this.valueStorageId = valueStorageId;
	}

}
