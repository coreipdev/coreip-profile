package com.group.coreip.models;

public enum Salutation {

	 Mr, Mrs, Miss, Ms, Dr, Prof, None;
	    public static Salutation getSalutation(String salutation) {
	        if (salutation == null) {
	            return None;
	        }
	        try {
	            return valueOf(salutation);
	        } catch (IllegalArgumentException e) {
	            return None;
	        }
	    }

}
