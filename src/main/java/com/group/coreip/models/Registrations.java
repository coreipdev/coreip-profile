package com.group.coreip.models;

public class Registrations {

	private int secondsToExpire;
	private String status;
	private String primary;
	private String uri;
	private long expires;
	private String identity;
	private String contact;
	private String instrument;
	private String regCallId;
	
	public int getSecondsToExpire() {
		return secondsToExpire;
	}
	
	public void setSecondsToExpire(int secondsToExpire) {
		this.secondsToExpire = secondsToExpire;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getPrimary() {
		return primary;
	}
	
	public void setPrimary(String primary) {
		this.primary = primary;
	}
	
	public String getUri() {
		return uri;
	}
	
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public long getExpires() {
		return expires;
	}
	
	public void setExpires(long expires) {
		this.expires = expires;
	}
	
	public String getIdentity() {
		return identity;
	}
	
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	
	public String getContact() {
		return contact;
	}
	
	public void setContact(String contact) {
		this.contact = contact;
	}
	
	public String getInstrument() {
		return instrument;
	}
	
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	
	public String getRegCallId() {
		return regCallId;
	}
	
	public void setRegCallId(String regCallId) {
		this.regCallId = regCallId;
	}
	
	
	/*	String extensionNO;
	String contacts;
	String expires;
	String identity;
	String isReg;
	
	public Registrations(String extensionNO, String contacts, String expires, String identity, String isReg) {
		super();
		this.extensionNO = extensionNO;
		this.contacts = contacts;
		this.expires = expires;
		this.identity = identity;
		this.isReg = isReg;
	}
	
	public String getExtensionNO() {
		return extensionNO;
	}
	public void setExtensionNO(String extensionNO) {
		this.extensionNO = extensionNO;
	}
	public String getContacts() {
		return contacts;
	}
	public void setContacts(String contacts) {
		this.contacts = contacts;
	}
	public String getExpires() {
		return expires;
	}
	public void setExpires(String expires) {
		this.expires = expires;
	}
	public String getIdentity() {
		return identity;
	}
	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getIsReg() {
		return isReg;
	}

	public void setIsReg(String isReg) {
		this.isReg = isReg;
	}*/
}
