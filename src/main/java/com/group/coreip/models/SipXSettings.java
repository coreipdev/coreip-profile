package com.group.coreip.models;

public class SipXSettings {
	private String value_storage_id;
	private String value;
	private String path;
	
	public String getValue_storage_id() {
		return value_storage_id;
	}
	public void setValue_storage_id(String value_storage_id) {
		this.value_storage_id = value_storage_id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}
