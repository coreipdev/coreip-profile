package com.group.coreip.fileIO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.group.coreip.exceptions.CoreException;
import com.group.coreip.logging.LogValues;
import com.group.coreip.logging.Logger;
import com.group.coreip.utils.CacheData;

@Service
public class FileOperationImpl implements FileOperation {

	
	public BufferedReader uploadFile(MultipartFile file) {
		
		Logger.sysLog(LogValues.info, this.getClass().getName(), "Starting uploadFile() function.......");
		
		BufferedReader reader = null;
		
		try {
			
			final InputStream inputStream = file.getInputStream();
			
			reader = new BufferedReader(new InputStreamReader(inputStream));
			
		} catch(Exception ex) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), "Error while processing MultipartFile file -> " + CoreException.GetStack(ex));
		}
		
		Logger.sysLog(LogValues.info, this.getClass().getName(), "uploadFile() function ended......");
		return reader;
	}

	@Override
	public String readFromFile(String fileName) {
		// TODO Auto-generated method stub
		try {
			String data = "";
			String currentLine = "";
			FileReader fr = new FileReader(fileName);
			BufferedReader bufReader = new BufferedReader(fr);
			while ((currentLine = bufReader.readLine()) != null) {
				data = data + currentLine;
				data = data + "\n";
			}
			bufReader.close();
			fr.close();
			return data;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	public boolean overrideWriteToFile(String fileName, String data) {
		// TODO Auto-generated method stub
		try {
			FileWriter fw = new FileWriter(fileName, false);
			fw.write(data);
			fw.flush();
			fw.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	
	@Override
	public boolean writeToFile(String fileName, String data) {
		// TODO Auto-generated method stub
		try {
			FileWriter fw = new FileWriter(fileName);
			fw.write(data);
			fw.flush();
			fw.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	@Override
	public boolean writeFile(MultipartFile file) throws IOException {
		
		byte[] bytes = file.getBytes();
		Path path = Paths.get( CacheData.getConstantData("filePath") + file.getOriginalFilename());

        Files.write(path, bytes);
		
		return true;
	}
	

	@Override
	public boolean writeRestoreFile(MultipartFile file) throws IOException {
		
		byte[] bytes = file.getBytes();
		Path path = Paths.get( "/tmp/" + file.getOriginalFilename());

        Files.write(path, bytes);
		
		return true;
	}
	
	
	@Override
	public boolean writeFile(MultipartFile file, String fileName) throws IOException {
		
		byte[] bytes = file.getBytes();
		Path path = Paths.get(CacheData.getConstantData("filePath") + fileName);

        Files.write(path, bytes);
		
		return true;

	}
	
}
