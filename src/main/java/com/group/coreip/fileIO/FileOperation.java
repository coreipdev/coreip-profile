package com.group.coreip.fileIO;

import java.io.BufferedReader;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public interface FileOperation {

	public BufferedReader uploadFile(MultipartFile file);
	public boolean writeToFile(String fileName, String Data);
	
	public boolean writeFile(MultipartFile file) throws IOException;
	boolean writeFile(MultipartFile file, String fileName) throws IOException;
	boolean writeRestoreFile(MultipartFile file) throws IOException;
	String readFromFile(String fileName);
	boolean overrideWriteToFile(String fileName, String data);
}
