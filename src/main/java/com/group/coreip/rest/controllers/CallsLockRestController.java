package com.group.coreip.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.group.coreip.models.SipXSettings;
import com.group.coreip.models.SipxUserInfo;
import com.group.coreip.repositories.SipXConfigRepository;
import com.group.coreip.services.CallsLockService;
import com.group.coreip.services.ProfileService;

@RestController
@RequestMapping(value = "/sipxip/", produces = MediaType.APPLICATION_JSON_VALUE)
public class CallsLockRestController extends BaseController {

	@Autowired
	private CallsLockService callsLockService;

	@RequestMapping(value = "updateusers", method = RequestMethod.GET)
	public ResponseEntity<String> updateUsers() {

		String response = "";
		HttpStatus status = HttpStatus.OK;

		if (callsLockService.updateUsers()) {
			response = "Success";
		} else {
			response = "Failed";
		}

		if (StringUtils.isEmpty(response)) {
			response = "Data not Found";
			status = HttpStatus.NOT_FOUND;
		}

		return new ResponseEntity<String>(response, status);
	}

	@RequestMapping(value = "callslock", method = RequestMethod.GET)
	public ResponseEntity<String> callslock(@RequestParam("caller") String caller,
			@RequestParam("password") String password, @RequestParam("permission") String permission) {

		String response = "";
		HttpStatus status = HttpStatus.OK;
		System.out.println("Calls Lock requested by "+caller+" for permission "+permission);
		response = callsLockService.callsLock(caller, password, permission);

		if (StringUtils.isEmpty(response)) {
			response = "Data not Found";
			status = HttpStatus.NOT_FOUND;
		}

		return new ResponseEntity<String>(response, status);
	}

	@RequestMapping(value = "callsunlock", method = RequestMethod.GET)
	public ResponseEntity<String> callsunlock(@RequestParam("caller") String caller,
			@RequestParam("password") String password, @RequestParam("permission") String permission) {

		String response = "";
		HttpStatus status = HttpStatus.OK;
		System.out.println("Calls UnLock requested by "+caller+" for permission "+permission);
		response = callsLockService.callsUnlock(caller, password, permission);

		if (StringUtils.isEmpty(response)) {
			response = "Data not Found";
			status = HttpStatus.NOT_FOUND;
		}

		return new ResponseEntity<String>(response, status);
	}
}
