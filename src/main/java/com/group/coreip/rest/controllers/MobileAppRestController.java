package com.group.coreip.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.group.coreip.models.SipxUserInfo;
import com.group.coreip.models.UserProfile;
import com.group.coreip.services.ProfileService;
import com.group.coreip.utils.Utility;

@RestController
@RequestMapping(value = "/sipxip/", produces = MediaType.APPLICATION_JSON_VALUE)
public class MobileAppRestController extends BaseController {

	@Autowired
	private ProfileService profileService;
	
	@RequestMapping(value = "signupApp", method = RequestMethod.GET)
	public ResponseEntity<String> getPassword(@RequestParam("mailID") String mailID,
			@RequestParam("firstName") String name, @RequestParam("mobileNo") String mobileNo) {
		
		String response = "";
		HttpStatus status = HttpStatus.OK; 
		UserProfile sipxUserProfile = null;
		SipxUserInfo sipxUserInfo = null;
		
		sipxUserProfile = profileService.fetchSipxUserProfilebyMob(mobileNo);
		if (null != sipxUserProfile) { 
			System.out.println("Data read!!!! " + sipxUserProfile.getUserName() );
		}
		if (null != sipxUserProfile) {
			sipxUserInfo = profileService.fetchSipXUserInfo(sipxUserProfile.getUserName());
			if (null != sipxUserInfo) {
				response = Utility.convertObjectToJsonStr(sipxUserInfo);
			}
		}
		
		if(StringUtils.isEmpty(response)) {
			response = "Data not Found";
			status = HttpStatus.NOT_FOUND;
		}
		
		return new ResponseEntity<String>(response, status);
	}
	
	@RequestMapping(value = "getUserCredentials", method = RequestMethod.GET)
	public ResponseEntity<String> getUserCredentials(@RequestParam("username") String username) {
		
		String response = "";
		HttpStatus status = HttpStatus.OK; 
		SipxUserInfo sipxUserInfo = null;
		
		
		sipxUserInfo = profileService.fetchSipXUserInfo(username);
		if (null != sipxUserInfo) {
			response = Utility.convertObjectToJsonStr(sipxUserInfo);
		}
		
		if(StringUtils.isEmpty(response)) {
			response = "Data not Found";
			status = HttpStatus.NOT_FOUND;
		}
		
		return new ResponseEntity<String>(response, status);
	}
}
