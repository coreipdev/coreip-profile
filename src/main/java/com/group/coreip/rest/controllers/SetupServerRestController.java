package com.group.coreip.rest.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.group.coreip.exceptions.CoreException;
import com.group.coreip.fileIO.FileOperation;
import com.group.coreip.fileIO.FileOperationImpl;
import com.group.coreip.logging.LogValues;
import com.group.coreip.logging.Logger;
import com.group.coreip.models.CacheDataInfo;
import com.group.coreip.repositories.StaticDataDao;
import com.group.coreip.rest.utils.RestStatus;
import com.group.coreip.services.UcxInterfaceService;
import com.group.coreip.utils.CacheData;
import com.group.coreip.utils.Utility;

@RestController
public class SetupServerRestController extends BaseController {

	@Autowired
	StaticDataDao staticDataDao;
	
	@Autowired
	FileOperation ioService;
	
	@Autowired
	CacheData cacheData;
	
	@Autowired
	UcxInterfaceService ucxInterfaceService;
	
	@RequestMapping(value = "/reloadCache")
	public String reloadCache()
	{
		try
		{
			ucxInterfaceService.getUcxUsers();
			cacheData.loadCache();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return "Cache reloaded";
	}
	
	@RequestMapping(value = "/createConstantData")
	public String createConstantData(@RequestParam String data, Map<String, Object> model) {
		System.out.println("createConstantData in controller");
		System.out.println(data);

		String jsons[] =  data.split("#");
		
		for( int i = 0; i <= jsons.length - 1; i++) {
			CacheDataInfo cacheDataInfo = Utility.convertJsonStrToObject(jsons[i], CacheDataInfo.class);
			System.out.println("Key : "+ cacheDataInfo.getKEY() +" Value : "+cacheDataInfo.getVALUE() );
			staticDataDao.insertConstantData(cacheDataInfo, "coreIP_ConstantDataInfo");
			CacheData.updateContantData(cacheDataInfo);
		}
		
		return "";
	}
	
	@RequestMapping(value = "/restoreData", method = RequestMethod.POST)
	public ResponseEntity<RestStatus<Boolean>> restoreData(@RequestParam("fileToUpload") MultipartFile file) 
	{

		Logger.sysLog(LogValues.info, this.getClass().getName(), "******************* Restoring data ******************");

		if (file == null || file.isEmpty()) {
			return new ResponseEntity<RestStatus<Boolean>>(new RestStatus<Boolean>(HttpStatus.BAD_REQUEST.value(), ""),
					HttpStatus.BAD_REQUEST);
		}
		FileOperation operation = new FileOperationImpl();
		String scriptName = "/etc/as7880/webapps/configure/WEB-INF/classes/scriptFiles/restoreConfigureData.sh /tmp/" + file.getOriginalFilename();
		Runtime rt = Runtime.getRuntime();
		Process process = null;
		
		long fileSize = file.getSize();
		String fileName = file.getOriginalFilename();
		
		Logger.sysLog(LogValues.info, this.getClass().getName(),
				"File to read  - File Name: " + fileName + ", File Size: " + fileSize);
		
		try {
			operation.writeRestoreFile(file);
			
			process = rt.exec(scriptName);
			process.waitFor();
			System.out.println("Done with Configure restore Script");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return new ResponseEntity<RestStatus<Boolean>>(
				new RestStatus<Boolean>(HttpStatus.OK.value(), "Data Restored Successfully!!"), HttpStatus.OK);
	   
	}
	
	@RequestMapping(value = "/updateConstantData")
	public String updateConstantData(@RequestParam String data, Map<String, Object> model) {
		System.out.println("updateConstantData in controller");
		System.out.println(data);

		
			CacheDataInfo cacheDataInfo = Utility.convertJsonStrToObject(data, CacheDataInfo.class);
			System.out.println("Key : "+ cacheDataInfo.getKEY() +" Value : "+cacheDataInfo.getVALUE() );
			staticDataDao.updateStaticData(cacheDataInfo, "coreIP_ConstantDataInfo");
			CacheData.updateContantData(cacheDataInfo);
		return "";
	}
	
	
	@RequestMapping(value = "/findVersion")
	public ResponseEntity<RestStatus<String>> findVersion(Map<String, Object> model) {

		String ver = CacheData.sipXecsBridge;
		
		RestStatus<String> response = null;
		String message = "";
		HttpStatus status = HttpStatus.OK;
		
		if (ver == null) {
			status = HttpStatus.NOT_FOUND;
			message = "Unable to find Version";
		}

		response = new RestStatus(status.value(), message, ver);

		return new ResponseEntity<RestStatus<String>>(response, status);
		
	}
	
	
	
	@RequestMapping(value = "/getConstantDataInfo")
	public ResponseEntity<RestStatus<CacheDataInfo>> getConstantData(@RequestParam String Key, Map<String, Object> model) {
		System.out.println("getConstantData in controller");
		System.out.println(Key);

		//String jsons[] =  data.split("#");
		
		CacheDataInfo info = CacheData.getConstantDataInfo(Key);
		
		RestStatus<CacheDataInfo> response = null;
		String message = "";
		HttpStatus status = HttpStatus.OK;
		
		if (info == null) {
			status = HttpStatus.NOT_FOUND;
			message = "Unable to find Data for Key "+Key;
		}

		response = new RestStatus(status.value(), message, Key, info);

		return new ResponseEntity<RestStatus<CacheDataInfo>>(response, status);
	}
	
	@RequestMapping(value = "/uploadSetupCSV", method = RequestMethod.POST)
	public ResponseEntity<RestStatus<Boolean>> uploadSetupCSVFile(@RequestParam("fileToUpload") MultipartFile file) {

		Logger.sysLog(LogValues.info, this.getClass().getName(), "Start");

		if (file == null || file.isEmpty()) {
			return new ResponseEntity<RestStatus<Boolean>>(new RestStatus<Boolean>(HttpStatus.BAD_REQUEST.value(), ""),
					HttpStatus.BAD_REQUEST);
		}
		long fileSize = file.getSize();
		String fileName = file.getOriginalFilename();

		Logger.sysLog(LogValues.info, this.getClass().getName(),
				"File to read  - File Name: " + fileName + ", File Size: " + fileSize);

		BufferedReader reader = ioService.uploadFile(file);
		
		
			String line = "";
			try {
				
				while ((line = reader.readLine()) != null) {
					
					String[] fileData = line.split(",");
					
					if(fileData[0]==null || fileData[0].isEmpty() || fileData[1]==null || fileData[1].isEmpty() ||
					   fileData[2]==null || fileData[2].isEmpty())
					{
						Logger.sysLog(LogValues.error, this.getClass().getName(), 
								"Entry passed : "+line);
						continue;
					}
					
					CacheDataInfo cacheInfo = new CacheDataInfo();
					
					cacheInfo.setKEY(fileData[0]);
					cacheInfo.setNAME(fileData[1]);
					cacheInfo.setVALUE(fileData[2]);
					staticDataDao.updateStaticData(cacheInfo, "coreIP_ConstantDataInfo");
					CacheData.updateContantData(cacheInfo);
				}
				
			} catch (IOException e) {
				Logger.sysLog(LogValues.error, this.getClass().getName(), "Error while reading file " + CoreException.GetStack(e));
			}

		return new ResponseEntity<RestStatus<Boolean>>(
				new RestStatus<Boolean>(HttpStatus.OK.value(), "Data Uploaded Successfully!!"), HttpStatus.OK);
	   }


}
