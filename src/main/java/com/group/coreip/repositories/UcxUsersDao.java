package com.group.coreip.repositories;

import java.util.ArrayList;
import java.util.List;

import com.group.coreip.models.CacheDataInfo;
import com.group.coreip.models.LoginInfo;
import com.group.coreip.models.User;

public interface UcxUsersDao {
	boolean insertUser(User user);

	ArrayList<User> fetchAllUsers();

	User fetchUserByExtension(String extensionNo);

	User fetchUserBySipxId(String sipxId);

	boolean updateUser(User user);

	boolean deleteUser(String extensionNo);
}
