package com.group.coreip.repositories;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.group.coreip.models.SipXSettings;
import com.group.coreip.models.SipxUserInfo;
import com.group.coreip.utils.Utility;

@Repository
public class SipXConfigRepositoryImpl implements SipXConfigRepository {

	@Value("${coreip.postgress.host}")
	private String database;

	@Value("${coreip.postgress.username}")
	private String postgressUser;

	@Value("${coreip.postgress.password}")
	private String postgressPassword;

	@Override
	public SipxUserInfo fetchSipXUserInfo(String username) {

		SipxUserInfo sipxUserInfo = new SipxUserInfo();
		Statement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		conn = Utility.getPostgresConnection("jdbc:postgresql://" + database + ":5432/SIPXCONFIG", postgressUser,
				postgressPassword);
		if (conn != null) {
			try {
				stmt = conn.createStatement();
				stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery("Select * from users where user_name like '" + username + "'");
				rs.afterLast();
				while (rs.previous()) {
					sipxUserInfo.setUser_id(rs.getString("user_id"));
					sipxUserInfo.setfName(rs.getString("first_name"));
					sipxUserInfo.setlName(rs.getString("last_name"));
					sipxUserInfo.setPintoken(rs.getString("pintoken"));
					sipxUserInfo.setSipPasswd(rs.getString("sip_password"));
					sipxUserInfo.setValueStorageId(rs.getString("value_storage_id"));
					sipxUserInfo.setUsername(username);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return sipxUserInfo;
	}

	@Override
	public ArrayList<SipXSettings> fetchSipXSettings(String userid) {

		SipXSettings sipxUserSetting = new SipXSettings();
		ArrayList<SipXSettings> settingList = new ArrayList<SipXSettings>();
		Statement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		conn = Utility.getPostgresConnection("jdbc:postgresql://" + database + ":5432/SIPXCONFIG", postgressUser,
				postgressPassword);
		if (conn != null) {
			try {
				stmt = conn.createStatement();
				stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery("Select * from setting_value where value_storage_id like '" + userid + "'");
				rs.afterLast();
				while (rs.previous()) {
					sipxUserSetting.setValue_storage_id(rs.getString("value_storage_id"));
					sipxUserSetting.setValue(rs.getString("value"));
					sipxUserSetting.setPath(rs.getString("path"));
					settingList.add(sipxUserSetting);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return settingList;
	}

	@Override
	public boolean updateSipXSetting(SipXSettings userSetting, String valueToInsert) {

		Statement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String st = null;
		conn = Utility.getPostgresConnection("jdbc:postgresql://" + database + ":5432/SIPXCONFIG", postgressUser,
				postgressPassword);
		if (conn != null) {
			try {
				stmt = conn.createStatement();
				stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				st = "Select * from setting_value where value_storage_id = " + userSetting.getValue_storage_id()
						+ " and path like '" + userSetting.getPath() + "'";
				rs = stmt.executeQuery(st);
				rs.afterLast();
				if (rs.previous()) {
					if (rs.getString("value").equalsIgnoreCase(userSetting.getValue())) {
						return true;
					} else {
						st = "delete from setting_value orders where value_storage_id = "
								+ userSetting.getValue_storage_id() + " and path like '" + userSetting.getPath() + "'";
						System.out.println(st);
						rs = stmt.executeQuery(st);
					}
				}
				if (valueToInsert.equalsIgnoreCase(userSetting.getValue())) {
					st = "insert into setting_value (value_storage_id, value, path) values ("
							+ userSetting.getValue_storage_id() + ", '" + userSetting.getValue() + "', '"
							+ userSetting.getPath() + "')";

					System.out.println(st);
					rs = stmt.executeQuery(st);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return true;
	}

}
