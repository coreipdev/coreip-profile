package com.group.coreip.repositories;

import java.util.ArrayList;

import com.group.coreip.models.SipXSettings;
import com.group.coreip.models.SipxUserInfo;


public interface SipXConfigRepository {

	public SipxUserInfo fetchSipXUserInfo (String username);
	public ArrayList<SipXSettings> fetchSipXSettings(String userid);
	boolean updateSipXSetting(SipXSettings userSetting, String valueToInsert);
}
