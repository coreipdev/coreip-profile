package com.group.coreip.repositories;

import java.util.List;

import com.group.coreip.models.CacheDataInfo;

public interface StaticDataDao {
	List<CacheDataInfo> fetchStaticData(String collection);
	public boolean updateStaticData(CacheDataInfo info, String dbCollection);
	boolean enableGeneralStaticData(String key, String dbCollection);
	boolean disableGeneralStaticData(String key, String dbCollection);
	boolean insertConstantData(CacheDataInfo info, String collectionName);
}
