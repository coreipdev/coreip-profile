package com.group.coreip.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.group.coreip.models.UserProfile;

public interface UserProfileMongoRepository extends MongoRepository<UserProfile, String> {

	@Query(value = "{ 'mobileNumber' : ?0 }", fields = "{ 'm_cellPhoneNumber': 1 }")
	public UserProfile getProfileByMobileNumber(String mobileNumber);
}
