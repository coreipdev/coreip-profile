package com.group.coreip.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.group.coreip.exceptions.CoreException;
import com.group.coreip.logging.LogValues;
import com.group.coreip.logging.Logger;
import com.group.coreip.models.LoginInfo;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

@Repository
public class LoginDaoImpl implements LoginDao {
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	@Override
	public boolean insertLoginInfo(LoginInfo info) {
		boolean flag = false;
		Logger.sysLog(LogValues.debug, this.getClass().getName(), "Login Info  for user  "
				+ info.getUserName() /*" with Account ID = " + info.getUserName()*/);
		try {
			mongoTemplate.insert(info, "coreIP_LoginInfo");
			flag = true;
		} catch (Exception ex) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(ex));
		}
		return flag;
	}

	@Override
	public List<LoginInfo> fetchAllLoginInfo()
	{
		List<LoginInfo> info = new ArrayList<LoginInfo>();
		try 
		{
			Query query = new Query();
			List<LoginInfo> list = mongoTemplate.find(query, LoginInfo.class, "coreIP_LoginInfo");
			Logger.sysLog(LogValues.debug, this.getClass().getName(), "Login Info list size = " + list.size());
			return list;
		} catch (Exception e) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(e));
		}
		return info;
	}
	
	@Override
	public LoginInfo fetchLoginInfo(String userName) {
		LoginInfo info = null;
		try	
		{
			Query query = new Query(Criteria.where("userName").is(userName));
			info = mongoTemplate.findOne(query,LoginInfo.class, "coreIP_LoginInfo");
			Logger.sysLog(LogValues.error, this.getClass().getName(), "Fetching USer Info " + userName  );
			
			if(info!=null)
			{
				Logger.sysLog(LogValues.error, this.getClass().getName(), "Fetching User Info " +info.getUserID() +" " +info.getUserName() +" " +info.getPassWord() );
			}
			else
			{
				Logger.sysLog(LogValues.error, this.getClass().getName(), "User Info is null " );

			}	
		}
		catch(Exception e)	{
			
		}
		return info;
	} //  end of fetchLoginInfo method
	
	
	
	@Override
	public LoginInfo fetchLoginInfoByResetID(String resetId) {
		LoginInfo info = null;
		try	{
			Query query = new Query(Criteria.where("resetID").is(resetId));
			info = mongoTemplate.findOne(query,LoginInfo.class, "coreIP_LoginInfo");
			Logger.sysLog(LogValues.error, this.getClass().getName(), "Fetching User Info for" +resetId +" " +info.getUserName());
			Logger.sysLog(LogValues.error, this.getClass().getName(), "Fetching User Info " +info.getUserName() +" " +info.getPassWord());
		}
		catch(Exception e)	{
			
		}
		return info;
	}


	@Override
	public boolean updateLoginInfo(LoginInfo info) {
		// TODO Auto-generated method stub
		Logger.sysLog(LogValues.debug, this.getClass().getName(), "Update LoginInfo for ::" + info.getUserName());
		boolean flag = false;
		try {
			DBCollection collection = mongoTemplate.getCollection("coreIP_LoginInfo");
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.append("userName", info.getUserName());
			DBObject updateQuery = new BasicDBObject().append("userName", info.getUserName())
					.append("passWord", info.getPassWord()).append("emailId", info.getEmailId())
					.append("mobileNo", info.getMobileNo()).append("userType", info.getUserType())
					.append("resetID", info.getResetID());
					

			DBObject update = new BasicDBObject("$set", updateQuery);
			collection.update(searchQuery, update);
			flag = true;
		} catch (Exception ex) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(ex));
		}
		return flag;
	}

} // end of LoginDaoImpl class
