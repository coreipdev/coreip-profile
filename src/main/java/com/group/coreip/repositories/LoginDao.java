package com.group.coreip.repositories;

import java.util.List;

import com.group.coreip.models.LoginInfo;

public interface LoginDao {
	
	public LoginInfo fetchLoginInfo(String userName);
	public LoginInfo fetchLoginInfoByResetID(String resetId);
	boolean insertLoginInfo(LoginInfo info);
	boolean updateLoginInfo(LoginInfo info);
	public List<LoginInfo> fetchAllLoginInfo();

}
