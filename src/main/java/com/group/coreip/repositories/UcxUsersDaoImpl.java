package com.group.coreip.repositories;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.group.coreip.exceptions.CoreException;
import com.group.coreip.logging.LogValues;
import com.group.coreip.logging.Logger;
import com.group.coreip.models.LoginInfo;
import com.group.coreip.models.User;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

@Repository
public class UcxUsersDaoImpl implements UcxUsersDao {
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	@Override
	public boolean insertUser(User user) {
		boolean flag = false;
		Logger.sysLog(LogValues.debug, this.getClass().getName(), "User for extention inserted "
				+ user.getExtensionNo());
		try {
			mongoTemplate.insert(user, "coreIP_UcxUsers");
			flag = true;
		} catch (Exception ex) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(ex));
		}
		return flag;
	}

	@Override
	public ArrayList<User> fetchAllUsers()
	{
		ArrayList<User> userList = new ArrayList<User>();
		try 
		{
			Query query = new Query();
			List<User> list = mongoTemplate.find(query, User.class, "coreIP_UcxUsers");
			Logger.sysLog(LogValues.debug, this.getClass().getName(), "User Count = " + list.size());
			return userList;
		} catch (Exception e) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(e));
		}
		return userList;
	}
	
	@Override
	public boolean deleteUser(String extensionNo) {
		Logger.sysLog(LogValues.debug, this.getClass().getName(), "Delete User for extensionNo :: " + extensionNo);
		try {
			DBCollection collection = mongoTemplate.getCollection("coreIP_UcxUsers");
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.append("extensionNo", extensionNo);
			collection.remove(searchQuery);
			return true;
		} catch (Exception ex) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(ex));
		}
		return false;
	}
	
	@Override
	public User fetchUserByExtension(String extensionNo) {
		User user = null;
		try	
		{
			Query query = new Query(Criteria.where("extensionNo").is(extensionNo));
			user = mongoTemplate.findOne(query,User.class, "coreIP_UcxUsers");
			Logger.sysLog(LogValues.error, this.getClass().getName(), "Fetching user with extension" + extensionNo);
		}
		catch(Exception e)	{
			
		}
		return user;
	} //  end of fetchLoginInfo method
	
	
	
	@Override
	public User fetchUserBySipxId(String sipxId) {
		User user = null;
		try	{
			Query query = new Query(Criteria.where("sipxId").is(sipxId));
			user = mongoTemplate.findOne(query, User.class, "coreIP_UcxUsers");
		}
		catch(Exception e)	{
			
		}
		return user;
	}


	@Override
	public boolean updateUser(User user) {
		// TODO Auto-generated method stub
		Logger.sysLog(LogValues.debug, this.getClass().getName(), "Update User for ::" + user.getExtensionNo());
		boolean flag = false;
		try {
			DBCollection collection = mongoTemplate.getCollection("coreIP_UcxUsers");
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.append("extensionNo", user.getExtensionNo());
			DBObject updateQuery = new BasicDBObject().append("userID", user.getUserID())
					.append("range", user.getRange()).append("pin", user.getPin()).append("sipPassword", user.getSipPassword())
					.append("callerId", user.getCallerId()).append("firstName", user.getFirstName())
					.append("lastName", user.getLastName()).append("host", user.getHost())
					.append("disallow", user.getDisallow()).append("allow", user.getAllow())
					.append("context", user.getContext()).append("hassip", user.getHassip())
					.append("callwaiting", user.getCallwaiting()).append("nat", user.getNat())
					.append("transfer", user.getTransfer()).append("ipVersion", user.getIpVersion())
					.append("qualify", user.getQualify()).append("groups", user.getGroups())
					.append("contacts", user.getContacts()).append("expires", user.getExpires())
					.append("identity", user.getIdentity()).append("isRegistered", user.getIsRegistered())
					.append("superadminAccess", user.isSuperadminAccess()).append("localAuthentication", user.isLocalAuthentication())
					.append("changePINfromIVR", user.isChangePINfromIVR())
					.append("configurePersonalAutoAttendant", user.isConfigurePersonalAutoAttendant())
					.append("configureMusiconHold", user.isConfigureMusiconHold())
					.append("configureGroupMusiconHold", user.isConfigureGroupMusiconHold())
					.append("subscribetoPresence", user.isSubscribetoPresence())
					.append("dialing900", user.isDialing900()).append("attendantDirectory", user.isAttendantDirectory())
					.append("internationalDialing", user.isInternationalDialing()).append("localDialing", user.isLocalDialing())
					.append("longDistanceDialing", user.isLongDistanceDialing()).append("mobileDialing", user.isMobileDialing())
					.append("tollFree", user.isTollFree()).append("voiceMail", user.isVoiceMail())
					.append("recordSystemPrompts", user.isRecordSystemPrompts()).append("configPA", user.isConfigPA());
			
			DBObject update = new BasicDBObject("$set", updateQuery);
			collection.update(searchQuery, update);
			flag = true;
		} catch (Exception ex) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(ex));
		}
		return flag;
	}

}
