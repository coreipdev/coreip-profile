package com.group.coreip.repositories;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.group.coreip.exceptions.CoreException;
import com.group.coreip.logging.LogValues;
import com.group.coreip.logging.Logger;
import com.group.coreip.models.CacheDataInfo;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

@Repository
public class StaticDataDaoImpl implements StaticDataDao {

	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public List<CacheDataInfo> fetchStaticData(String collection) {

		try {
			Query query = new Query();
			List<CacheDataInfo> list = mongoTemplate.find(query, CacheDataInfo.class, collection);
			Logger.sysLog(LogValues.debug, this.getClass().getName(), collection + " list size = " + list.size());
			return list;
		} catch (Exception e) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(e));
		}
		return null;
	}

	
	@Override
	public boolean updateStaticData(CacheDataInfo info, String dbCollection) {
		Logger.sysLog(LogValues.debug, this.getClass().getName(),
				"Update Key :" + info.getKEY() + " Value : " + info.getVALUE());
		boolean flag = false;
		try {
			DBCollection collection = mongoTemplate.getCollection(dbCollection);
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.append("KEY", info.getKEY());
			DBObject updateQuery = new BasicDBObject().append("VALUE", info.getVALUE());
			DBObject update = new BasicDBObject("$set", updateQuery);
			collection.update(searchQuery, update);
			flag = true;
		} catch (Exception ex) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(ex));
		}
		return flag;
	}
	
	@Override
	public boolean enableGeneralStaticData(String key, String dbCollection) {
		Logger.sysLog(LogValues.debug, this.getClass().getName(),
				"Update Key :" + key );
		boolean flag = false;
		try {
			DBCollection collection = mongoTemplate.getCollection(dbCollection);
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.append("KEY", key);
			DBObject updateQuery = new BasicDBObject().append("COMMENTED", "false");
			DBObject update = new BasicDBObject("$set", updateQuery);
			collection.update(searchQuery, update);
			flag = true;
		} catch (Exception ex) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(ex));
		}
		return flag;
	}
	
	@Override
	public boolean disableGeneralStaticData(String key, String dbCollection) {
		Logger.sysLog(LogValues.debug, this.getClass().getName(),
				"Update Key :" + key );
		boolean flag = false;
		try {
			DBCollection collection = mongoTemplate.getCollection(dbCollection);
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.append("KEY", key);
			DBObject updateQuery = new BasicDBObject().append("COMMENTED", "true");
			DBObject update = new BasicDBObject("$set", updateQuery);
			collection.update(searchQuery, update);
			flag = true;
		} catch (Exception ex) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(ex));
		}
		return flag;
	}

	@Override
	public boolean insertConstantData(CacheDataInfo info, String collectionName) {
		
		boolean flag = false;
		
		Logger.sysLog(LogValues.debug, this.getClass().getName(),
				"Insert for Key :" + info.getKEY() + " Value : " + info.getVALUE());
		try {
			mongoTemplate.insert(info, collectionName);
			flag = true;
		} catch (Exception ex) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(ex));
		}
		return flag;

	}
}
