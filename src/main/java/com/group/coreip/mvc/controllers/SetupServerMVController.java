package com.group.coreip.mvc.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.group.coreip.exceptions.CoreException;
import com.group.coreip.fileIO.FileOperation;
import com.group.coreip.logging.LogValues;
import com.group.coreip.logging.Logger;
import com.group.coreip.models.CacheDataInfo;
import com.group.coreip.utils.CacheData;


@Controller
public class SetupServerMVController {
	
	@Autowired
	FileOperation ioService;
	
	@RequestMapping(value = "/constantDataDetails")
	public String constantDataDetails(@ModelAttribute("constantDataForm") CacheDataInfo constantData,
			Map<String, Object> model) {

		List<CacheDataInfo> constantDataList = null;
		Logger.sysLog(LogValues.info, this.getClass().getName(),
				"******************** Constant Data Details ********************");
		try {
			constantDataList = CacheData.getAllConstantData();
		} catch (Exception ex) {
			Logger.sysLog(LogValues.error, this.getClass().getName(), CoreException.GetStack(ex));

		}
		model.put("constantDataList", constantDataList);
		return "setup/constantData";
	}
	
	
	@RequestMapping(value = "/createBackup", method = RequestMethod.GET)
	public void createBackup(HttpSession session, HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {

		String scriptName = "/etc/as7880/webapps/configure/WEB-INF/classes/scriptFiles/configureBackupMongo.sh configureBackup";
		Runtime rt = Runtime.getRuntime();
		Process process = null;
		
		
			ServletOutputStream stream = null;
			FileInputStream input = null;
		
		try {
			process = rt.exec(scriptName);
			process.waitFor();
			System.out.println("Done with Script");
			stream = response.getOutputStream();
			stream = response.getOutputStream();
			File wav = new File("/var/backup/configure/configureBackup.tgz");
			response.setHeader("Content-Type", "application/zip");
			response.addHeader("Content-Disposition", "attachment; filename=" + "configureBackup.tgz");
			response.setContentLength((int) wav.length());
			input = new FileInputStream(wav);
			byte[] buf1 = new byte[4096];
			int len = 0;
			while ((len = input.read(buf1)) > 0) {
				// System.out.println("Len "+len);
				stream.write(buf1, 0, len);
			}
		  }catch (Exception e) {
		   e.printStackTrace();
		  }
	
	 	
	   
	}

	
	
	@RequestMapping(value = "/exportSetupCSV", method = RequestMethod.GET)
	public void exportSetupCSV(HttpSession session, HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {
		
		//String filename ="/etc/coreip/charingDump/countryDetails.csv";
		String filepath = "/tmp/";
		String filename = "initial_Setup.csv";
		String buffer = "";
		
		//List<CountryDetails> countryInfo = countryDetailsService.fetchCountryDetails();
		List<CacheDataInfo> constantDataList = CacheData.getAllConstantData();
		
		if(null == constantDataList) {
			buffer = "Key,Name,Value";
		} else {
		for (CacheDataInfo info : constantDataList) {
				Logger.sysLog(LogValues.info, this.getClass().getName(),
						info.getKEY()+ ","
								+ info.getNAME() + "," +  info.getVALUE());
				buffer = buffer.concat(info.getKEY() 
								+ ","
								+ info.getNAME() 
								+ "," 
								+  info.getVALUE() 
		    					+ "\n"
		    					);
		    }
		}
		Logger.sysLog(LogValues.info, this.getClass().getName(),"Data Printed successfully");
		boolean dataUpdateStatus = ioService.writeToFile(filepath+filename, buffer);
		Logger.sysLog(LogValues.info, this.getClass().getName(),"File is created");
		if (dataUpdateStatus) {
			ServletOutputStream stream = null;
			FileInputStream input = null;

			// System.out.println(filepath + filename);
			try {
				stream = response.getOutputStream();
				stream = response.getOutputStream();
				File wav = new File(filepath + filename);
				response.setContentType("text/csv");
				response.addHeader("Content-Disposition", "attachment; filename=" + filename);
				response.setContentLength((int) wav.length());
				input = new FileInputStream(wav);
				byte[] buf1 = new byte[4096];
				int len = 0;
				while ((len = input.read(buf1)) > 0) {
					// System.out.println("Len "+len);
					stream.write(buf1, 0, len);
				}
			} catch (Exception e) {
			}
		}
		
	   }
}


