package com.group.coreip.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.group.coreip.logging.LogValues;
import com.group.coreip.logging.Logger;
import com.group.coreip.models.CacheDataInfo;
import com.group.coreip.models.User;
import com.group.coreip.repositories.StaticDataDao;
import com.group.coreip.repositories.UcxUsersDao;

public class CacheData {

	@Autowired
	private StaticDataDao staticDataDao;
	
	@Autowired
	private SyncInitialDB syncInitialDB;
	
	private UcxUsersDao ucxUserDao;

	public static boolean isServerDeployed = true;
	private static List<CacheDataInfo> cacheConstantDataInfo;
	public static ArrayList<User> cacheUsers;

	public static String sipXecsBridge;

	/****************** Constant Utility ***********************/
	public static boolean updateContantData(CacheDataInfo info) {
		if ((cacheConstantDataInfo == null)) {
			Logger.sysLog(LogValues.info, CacheData.class.getName(), "Loading cacheConstantDataInfo from database");
			return false;
		}
		Logger.sysLog(LogValues.info, CacheData.class.getName(), " cacheConstantDataInfo key = " + info.getKEY()
				+ " , Name = " + info.getNAME() + ",  value = " + info.getVALUE());
		for (CacheDataInfo infodb : cacheConstantDataInfo) {
			if (infodb.getKEY().equalsIgnoreCase(info.getKEY())) {
				infodb.setVALUE(info.getVALUE());
			}
		}
		// invoke function to write file ftp.conf
		// prepareFtpConfigFile();
		return true;
	}

	public static String getConstantData(String key) {
		if ((cacheConstantDataInfo == null) || cacheConstantDataInfo.size() < 1) {
			Logger.sysLog(LogValues.info, CacheData.class.getName(), "Loading cacheConstantDataInfo from database");
			return null;
		}
		for (CacheDataInfo info : cacheConstantDataInfo) {
			if (info.getKEY().equalsIgnoreCase(key)) {
				Logger.sysLog(LogValues.info, CacheData.class.getName(),
						" cacheConstantDataInfo key =" + key + " ,  value = " + info.getVALUE());
				return "" + info.getVALUE();
			}
		}
		return null;
	} // End of method

	public static CacheDataInfo getConstantDataInfo(String key) {
		if ((cacheConstantDataInfo == null) || cacheConstantDataInfo.size() < 1) {
			Logger.sysLog(LogValues.info, CacheData.class.getName(), "Loading cacheConstantDataInfo from database");
			return null;
		}
		for (CacheDataInfo info : cacheConstantDataInfo) {
			if (info.getKEY().equalsIgnoreCase(key)) {
				Logger.sysLog(LogValues.info, CacheData.class.getName(),
						" cacheConstantDataInfo key =" + key + " ,  value = " + info.getVALUE());
				return info;
			}
		}
		return null;
	} // End of method


	public static List<CacheDataInfo> getAllConstantData() {
		if ((cacheConstantDataInfo == null) || cacheConstantDataInfo.size() < 1) {
			Logger.sysLog(LogValues.info, CacheData.class.getName(), "Loading cacheConstantDataInfo from database");
			return null;
		}

		return cacheConstantDataInfo;
	} // End of method


	@PostConstruct
	public void loadCache() {
		System.out.println("####  Bean initialize method ####  loadCache() !!!");
		String fileName = "";
		if (isServerDeployed == true) {
			fileName = "/etc/as7880/webapps/sipxbridge/WEB-INF/classes/initialDatabase.sh";
		} else {
			
			fileName = "/Users/ankit/Documents/codebase/stsWorkSpace/coreip-profile/src/main/resources/initialDatabase.sh";
		}
		File initialDb = new File(fileName);
		if (initialDb.isFile()) {
			if (isServerDeployed == true) {
				Runtime rt = Runtime.getRuntime();
				Process process = null;
				try {
					process = rt.exec("//bin/chmod +x /etc/as7880/webapps/sipxbridge/WEB-INF/classes/scriptFiles/*");
					process.waitFor();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			syncInitialDB.checkDb(initialDb);
		} else {
			System.out.println("####  File "+fileName+" Not Found!!!!");
		}
		sipXecsBridge = "3a368fbb98b477fbccfc8347686583c8377c6018";
		CacheData.cacheConstantDataInfo = staticDataDao.fetchStaticData("coreIP_ConstantDataInfo");
		if ((cacheConstantDataInfo == null) || cacheConstantDataInfo.size() < 1) {
			System.out.println("cacheConstantDataInfo is empty");
		} else {
			System.out.println("cacheConstantDataInfo is not null");
		}
		CacheData.cacheUsers = ucxUserDao.fetchAllUsers();
		if (CacheData.cacheUsers == null || CacheData.cacheUsers.size() < 1) {
			CacheData.cacheUsers = new ArrayList<User>();
		}
		System.out.println("####  Bean initialized method ####  loadCache() !!!");
	}

}
