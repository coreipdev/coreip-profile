package com.group.coreip.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.group.coreip.exceptions.CoreException;
import com.group.coreip.logging.LogValues;
import com.group.coreip.logging.Logger;

public class Utility {

	private static Gson gson = new Gson();
	private static ObjectMapper obj = new ObjectMapper();
	private static ObjectMapper mapper = new ObjectMapper();
	/*
	 * private static ConstantDataDao constantDataDao; private static StdInfoDao
	 * stdInfoDao;
	 */

	public synchronized static String convertObjectToJsonStr(Object object) {
		return gson.toJson(object);
	}

	public synchronized static <T> T convertJsonStrToObject(String json, Class<T> classOfT) {
		return gson.fromJson(json, classOfT);
	}

	public synchronized static <T> T convertObjToClass(Object obj, Class<T> classOfT) {
		return mapper.convertValue(obj, classOfT);
	}
	
	public static <T> T checkAttributes(T cacheDb, T cacheDataInfo, Class<T> classOfT) {
		// TODO Auto-generated method stub
		for (Field field : classOfT.getDeclaredFields()) {
			field.setAccessible(true);
			try {
				if ((field.get(cacheDb) == null)) {
					Logger.sysLog(LogValues.info, "Utility", "New attribute : " + field.getName());
					field.set(cacheDb, field.get(cacheDataInfo));
					Logger.sysLog(LogValues.info, "Utility",
							"Data in cache : " + Utility.convertObjectToJsonStr(cacheDb));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return cacheDb;
	}
	
	
	public static <T> boolean campareAndUpdate(T cacheDb, T cacheDataInfo, Class<T> classOfT) {
		// TODO Auto-generated method stub
		boolean updated = false;
		for (Field field : classOfT.getDeclaredFields()) {
			field.setAccessible(true);
			try {
				if ((field.get(cacheDb) != field.get(cacheDataInfo))) {
					Logger.sysLog(LogValues.info, "Utility", "New attribute : " + field.getName());
					field.set(cacheDb, field.get(cacheDataInfo));
					updated = true;
					Logger.sysLog(LogValues.info, "Utility",
							"Data in cache : " + Utility.convertObjectToJsonStr(cacheDb));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return updated;
	}
	
	public static String getCurrentDateTime(String dateFormat) {
		SimpleDateFormat sdfDate = new SimpleDateFormat(dateFormat);
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	public static int getRandomNumber(int min, int max) {
		return (int) Math.floor(Math.random() * (max - min + 1)) + min;
	}

	public static Date getCalenderDate() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		return cal.getTime();
	}

	public static int getCalenderMonth() {
		Calendar cal = Calendar.getInstance();

		return (cal.get(Calendar.MONTH));
	}

	public String callUrl(String httpurl) {
		String httpUrlResponse = "";
		try {
			// Logger.sysLog(LogValues.info, this.getClass().getName(),"HTTP url
			// = "+httpurl);
			URL url = new URL(httpurl);
			URLConnection uc = url.openConnection();
			HttpURLConnection con = (HttpURLConnection) uc;
			InputStream rd = con.getInputStream();
			int c = 0;
			while ((c = rd.read()) != -1) {
				httpUrlResponse += (char) c;
			}
			rd.close();
			con.disconnect();
		} catch (Exception e) {
			Logger.sysLog(LogValues.error, Utility.class.getName(), CoreException.GetStack(e));

		}
		return httpUrlResponse.trim();
	}

	public static Date addDays(Date d, long days) {
		d.setTime(d.getTime() + (days * 1000 * 60 * 60 * 24));
		return d;
	}

	public static Date substractDays(Date d, int days) {
		d.setTime(d.getTime() - (days * 1000 * 60 * 60 * 24));
		return d;
	}

	public static long differenceOfDays(Date d1, Date d2, boolean bothDatesInclusive) {
		long diff = 0;
		diff = (d1.getTime() / 1000 / 60 / 60 / 24) - (d2.getTime() / 1000 / 60 / 60 / 24);
		if (bothDatesInclusive) {
			return (diff + 1);
		} else {
			return (diff);
		}
	}
	
	public static long differenceOfsecs(Date d1, Date d2) {
		long diff = 0;
		diff = (d1.getTime() / 1000) - (d2.getTime() / 1000);
		return (diff);
	}

	public static Date getNextMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, 1);
		return cal.getTime();
	}
	
	public static Date getPreviousMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		return cal.getTime();
	}

	public static long noofdays(Date d) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		int days = cal.getActualMaximum(Calendar.DATE);

		return days;
	}

	public static Date getStringToDate(String dateStr, String format) {
		Date d = null;
		try {
			SimpleDateFormat dateFormat = null;
			if (format != null) {
				dateFormat = new SimpleDateFormat(format);
			}
			d = dateFormat.parse(dateStr);
		} catch (Exception ex) {
			Logger.sysLog(LogValues.error, "Utility", CoreException.GetStack(ex));
		}
		return d;
	}

	public static Date getDate(Date d) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);

		return cal.getTime();
	}

	public static Date addHours(Date d, int hours) {
		d.setTime(d.getTime() + hours * 1000 * 60 * 60);
		return d;
	}

	public static Date addMinutes(Date d, int minutes) {
		d.setTime(d.getTime() + minutes * 1000 * 60);
		return d;
	}

	public static String getCurrentDate(String dateFormat) {
		SimpleDateFormat sdfDate = new SimpleDateFormat(dateFormat);
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	public static String dateToString(Date date, String dateFormat) {
		SimpleDateFormat sdfDate = new SimpleDateFormat(dateFormat);
		String strDate = sdfDate.format(date);
		return strDate;
	}
	
	public static String convertDateFormat1 (String dateStr, String inputFormat, String expectedFormat) {
		Date d = getStringToDate(dateStr, inputFormat);
		if (d != null) {
			return dateToString(d, expectedFormat);
		}
		return null;
	}
	
	public static Date convertDateFormat2 (Date dateStr, String inputFormat, String expectedFormat) {
		//Date d = getStringToDate(dateStr, inputFormat);
		String ds = dateToString(dateStr, expectedFormat);
		if (ds != null) {
			return getStringToDate(ds, expectedFormat);
		}
		return null;
	}
	
	public static String extractExtension(String uri) {
		String[] extension;
		System.out.println(uri);
		extension = uri.split("sip:");
		extension = extension[1].split("@");
		return extension[0];
	}

	public static String hashCal(String type, String str) {
		System.out.println("....." + str);
		byte[] hashseq = str.getBytes();
		StringBuffer hexString = new StringBuffer();
		try {
			MessageDigest algorithm = MessageDigest.getInstance(type);
			algorithm.reset();
			algorithm.update(hashseq);
			byte messageDigest[] = algorithm.digest();

			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1)
					hexString.append("0");
				hexString.append(hex);
			}

		} catch (NoSuchAlgorithmException nsae) {

		}

		return hexString.toString();
	} // end of hashCal

	public static void sendPost(String urlParameters) throws Exception {

		String url = "https://test.payu.in/_payment.php/_payment";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// add reuqest header
		con.setRequestMethod("POST");
		// con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		/*
		 * String urlParameters =
		 * "key=gtKFFx&txnid=2344322344322345&amount=50&productinfo=tshirt100&firstname=krati&email=krati.agrawal210@gmail.com&phone=8171935646&surl=172.20.12.119:8008/Athome/success&furl=172.20.12.119:8008/Athome/failure&hash="
		 * + hash;
		 */
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		// System.out.println(response.toString());

	}

	public static void sendPut(String url, String urlParameters) throws Exception {

		// String url = "https://test.payu.in/_payment.php/_payment";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// add reuqest header
		con.setRequestMethod("PUT");
		// con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		/*
		 * String urlParameters =
		 * "key=gtKFFx&txnid=2344322344322345&amount=50&productinfo=tshirt100&firstname=krati&email=krati.agrawal210@gmail.com&phone=8171935646&surl=172.20.12.119:8008/Athome/success&furl=172.20.12.119:8008/Athome/failure&hash="
		 * + hash;
		 */
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'PUT' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		// System.out.println(response.toString());

	}

	public static String sendGet(String url) throws Exception {

		URL obj = new URL(url);
		try {
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };

			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("GET");

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());
			return response.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//Database connection
		public static Connection postgresConnection = null;

		public static Connection getPostgresConnection(String dbName, String username, String password) {

			try {
				Class.forName("org.postgresql.Driver");
				if (postgresConnection == null) {
					System.out.println("Creating Connection with DB");
					postgresConnection = DriverManager.getConnection(dbName, username, password);
				}
				return postgresConnection;
			} catch (Exception e) {
				if (postgresConnection != null)
					try {
						postgresConnection.close();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				e.printStackTrace();
				return null;
			}
		}
}
