package com.group.coreip.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.group.coreip.logging.LogValues;
import com.group.coreip.logging.Logger;
import com.group.coreip.models.CacheDataInfo;
import com.group.coreip.models.LoginInfo;
import com.group.coreip.repositories.LoginDao;
import com.group.coreip.repositories.StaticDataDao;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

@Repository
public class SyncInitialDB {
	@Autowired
	private StaticDataDao staticDataDao;
	
	@Autowired
	private LoginDao loginDao;

	private static List<CacheDataInfo> constantDbList = new ArrayList<CacheDataInfo>();
	private static List<LoginInfo> loginDbList = new ArrayList<LoginInfo>();

	private static List<CacheDataInfo> constantFileList = new ArrayList<CacheDataInfo>();
	private static List<LoginInfo> loginFileList = new ArrayList<LoginInfo>();
	
	private static boolean loginDb = false;

	MongoClient client = new MongoClient();
	MongoDatabase db = client.getDatabase("sipXBridge");

	public void checkDb(File initialDb) {
		// TODO Auto-generated method stub
		Process process = null;
		try {
			if (CacheData.isServerDeployed == true) {
				process = Runtime.getRuntime()
						.exec("mongodump --db sipXBridge -o /tmp/sipXBridgeUpgrade"
								+ Utility.getCurrentDateTime("DDMMYYYYHHmmss"));
				process.waitFor();
			}
			List<String> collectionNames = db.listCollectionNames().into(new ArrayList<String>());
			

			readFile(initialDb);
			readDb();
			syncInitialData();
			writeFileIntoDb(); // for cms voice prompts
			checkCacheForInitialDb();
			setPermissions();

			if (CacheData.isServerDeployed == true) {
				// remove initialDatabase.sh
				process = Runtime.getRuntime()
						.exec("/bin/rm -rf /etc/as7880/webapps/configure/WEB-INF/classes/initialDatabase.sh");
				process.waitFor();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void readFile(File initialDb) {
		// TODO Auto-generated method stub
		String fileName = initialDb.getName();
		System.out.println("Filename : " + fileName);
		String line = "";
		try {

			FileReader file = new FileReader(initialDb);
			BufferedReader reader = new BufferedReader(file);

			String className = "";
			while ((line = reader.readLine()) != null) {
				System.out.println("Line : " + line);
				if (line.contains("[") && line.contains("]")) {
					line = line.replace("[", "");
					line = line.replace("]", "");
					className = line;
				} else {
					if (className.equalsIgnoreCase("CacheDataInfo")) {
						CacheDataInfo cacheDataInfo = Utility.convertJsonStrToObject(line, CacheDataInfo.class);
						constantFileList.add(cacheDataInfo);
					} else if (className.equalsIgnoreCase("LoginInfo")) {
						LoginInfo loginInfo = Utility.convertJsonStrToObject(line, LoginInfo.class);
						loginFileList.add(loginInfo);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void readDb() {
		// TODO Auto-generated method stub
		constantDbList = staticDataDao.fetchStaticData("coreIP_ConstantDataInfo");
		loginDbList = loginDao.fetchAllLoginInfo();
	}

	private void syncInitialData() {
		// TODO Auto-generated method stub
		Logger.sysLog(LogValues.info, this.getClass().getName(),
				"Constant Data File list : " + constantFileList.size());

		// Sync static data
		if (constantFileList != null && constantFileList.size() > 0) {
			constantDbList = syncStaticData(constantDbList, constantFileList);
		}
		if (loginFileList != null && loginFileList.size() > 0) {
			boolean updated = syncLoginInfo();
			loginDb = updated;
		}
	}
	
	private boolean syncLoginInfo() {
		// TODO Auto-generated method stub
		boolean updated = false;

		if (loginDbList == null || loginDbList.size() < 1) {
			Logger.sysLog(LogValues.info, this.getClass().getName(), "List is empty");
			loginDbList = loginFileList;
			updated = true;
		} else {
			for (LoginInfo loginInfo : loginFileList) {
				LoginInfo loginData = fetchLoginInfo(loginInfo.getUserName());
				if (loginData == null) {
					Logger.sysLog(LogValues.info, this.getClass().getName(),
							"Adding new entry : " + loginInfo.getUserName());
					loginDbList.add(loginInfo);
					Logger.sysLog(LogValues.info, this.getClass().getName(),
							"Entry added : " + Utility.convertObjectToJsonStr(loginInfo));
					updated = true;
				} else {
					loginData = Utility.checkAttributes(loginData, loginInfo, LoginInfo.class);
					Logger.sysLog(LogValues.info, this.getClass().getName(),
							"Entry updated : " + Utility.convertObjectToJsonStr(loginData));
					updated = true;
				}
			}
		}
		return updated;
	}
	
	private LoginInfo fetchLoginInfo(String userName) {
		// TODO Auto-generated method stub
		for (LoginInfo loginInfo : loginDbList) {
			if (loginInfo.getUserName().equalsIgnoreCase(userName)) {
				Logger.sysLog(LogValues.info, this.getClass().getName(),
						"Login Info for user " + userName + " is present in Database");
				return loginInfo;
			}
		}
		return null;
	}

	private void writeFileIntoDb() {
		// TODO Auto-generated method stub
		// Sync constant data for values that has to be inserted only during new
		// deployment, any changes in these values will be done using
		// constructor
	}

	private void checkCacheForInitialDb() {
		// TODO Auto-generated method stub
		if (constantDbList != null) {
			updateStaticCacheData("coreIP_ConstantDataInfo", constantDbList);
		}
		if (loginDb) {
			db.getCollection("coreIP_LoginInfo").drop();
			for (LoginInfo loginData : loginDbList) {
				loginDao.insertLoginInfo(loginData);
			}
		}
	}

	private void updateStaticCacheData(String collectionName, List<CacheDataInfo> cacheDbList) {
		// TODO Auto-generated method stub
		db.getCollection(collectionName).drop();

		for (CacheDataInfo constantData : cacheDbList) {
			staticDataDao.insertConstantData(constantData, collectionName);
		}
	}



	private List<CacheDataInfo> syncStaticData(List<CacheDataInfo> cacheDbList, List<CacheDataInfo> cacheFileList) {
		// TODO Auto-generated method stub
		boolean updated = false;
		if (cacheDbList == null || cacheDbList.size() < 1) {
			Logger.sysLog(LogValues.info, this.getClass().getName(), "List is empty");
			cacheDbList = cacheFileList;
			updated = true;
		} else {
			for (CacheDataInfo cacheDataInfo : cacheFileList) {
				CacheDataInfo cacheDataInfoDb = fetchConstantDetailsByKey(cacheDataInfo.getKEY());
				if (cacheDataInfoDb == null) {
					cacheDbList.add(cacheDataInfo);
					updated = true;
				} else {
					Logger.sysLog(LogValues.info, this.getClass().getName(),
							"Data in DB : " + Utility.convertObjectToJsonStr(cacheDataInfoDb));
					Logger.sysLog(LogValues.info, this.getClass().getName(),
							"Data in File : " + Utility.convertObjectToJsonStr(cacheDataInfo));
					cacheDataInfoDb = Utility.checkAttributes(cacheDataInfoDb, cacheDataInfo, CacheDataInfo.class);
					Logger.sysLog(LogValues.info, this.getClass().getName(),
							"Entry updated : " + Utility.convertObjectToJsonStr(cacheDataInfoDb));
					updated = true;
				}
			}
		}
		if (updated) {
			return cacheDbList;
		}
		return null;
	}

	private CacheDataInfo fetchConstantDetailsByKey(String key) {
		// TODO Auto-generated method stub
		for (CacheDataInfo cacheDataInfo : constantDbList) {
			if (cacheDataInfo.getKEY().equalsIgnoreCase(key)) {
				return cacheDataInfo;
			}
		}
		return null;
	}



	private void setPermissions() {
		// TODO Auto-generated method stub
		try {
			Process process = Runtime.getRuntime()
					.exec("/bin/chmod -R +x /etc/as7880/webapps/configure/WEB-INF/classes/scriptFiles");
			process.waitFor();
			Logger.sysLog(LogValues.info, this.getClass().getName(), "Setting permissions for script files");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
