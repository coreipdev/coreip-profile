package com.group.coreip.auth.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final Logger logger = LoggerFactory.getLogger(SecurityConfiguration.class);
	
	@Autowired
    private UserDetailsService userDetailsService;
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder authenticationMgr) throws Exception {
		authenticationMgr.userDetailsService(userDetailsService);
	}
     
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		logger.info("Start Security Configuration method - configure() - Start");
    	
		http
		.authorizeRequests()
			.antMatchers("/*").permitAll()
		.and()
		.formLogin()
			.loginPage("/login")
			.defaultSuccessUrl("/default")
			.failureUrl("/login?error")
			.usernameParameter("username").passwordParameter("password")				
		.and()
		.logout()
			.logoutSuccessUrl("/login?logout")
		.and()
			.csrf().disable();
    }
}
