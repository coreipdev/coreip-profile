package com.group.coreip.auth.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.group.coreip.services.LogonService;

@Controller
public class LogonController {

	@Autowired
	private SecurityService securityService;
	
    @Autowired
    LogonService loginService;

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String welcomePage(HttpServletRequest request) {

		if (securityService.isUserAuthenticated()) {
			return "redirect:/default";
		}

		return "redirect:/login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView loginPage(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid Credentials provided.");
		}

		if (logout != null) {
			model.addObject("message", "Logged out from JournalDEV successfully.");
		}

		model.setViewName("login/login");
		return model;
	}
	
	
	@RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
	public String resetPassword(@RequestParam String resetID, HttpSession session, ModelMap model) {
		model.put("resetID", resetID);
		return "login/getNewPassword";
	}
	
	@RequestMapping(value = "/savePassword", method = RequestMethod.GET)
	public String savePassword(@RequestParam String resetID, @RequestParam String password1, HttpSession session, ModelMap model) {
		loginService.savePassword(resetID, password1);		
		return "login/login";
	}

	@RequestMapping("/default")
	public String defaultAfterLogin(HttpServletRequest request) {

		if (request.isUserInRole("ROLE_ADMIN")) {
			request.getUserPrincipal().getName();
		}

		if (request.isUserInRole("ROLE_ADMIN")) {
			request.getSession().setAttribute("userName", securityService.findLoggedInUsername() );
			request.getSession().setAttribute("userRole", "ROLE_ADMIN");
			return "redirect:/constantDataDetails";
		}
		else if(request.isUserInRole("ROLE_DB"))
		{
			request.getSession().setAttribute("userRole", "ROLE_DB");
			return "redirect:/constantDataDetails";
		}
		if (request.isUserInRole("ROLE_RAILWAY_ADMIN")) {
			request.getSession().setAttribute("userName", securityService.findLoggedInUsername() );
			request.getSession().setAttribute("userRole", "ROLE_RAILWAY_ADMIN");
			return "redirect:/constantDataDetails";
		}
		else {
			
			/*
			String accountID = loginService.getAccountIdByUser(securityService.findLoggedInUsername());
			request.getSession().setAttribute("accountID", accountID);
			*/
			
			request.getSession().setAttribute("userName", securityService.findLoggedInUsername() );
			request.getSession().setAttribute("userRole", "ROLE_USER");
			return "redirect:/constantDataDetails";
		}
	}

}
