package com.group.coreip.auth.service;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.group.coreip.models.LoginInfo;
import com.group.coreip.services.LogonService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
	
	@Autowired
	private LogonService logonService;
	
	@Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		

		LoginInfo dbUser = null;
		dbUser = logonService.fetchLoginInfo(username);
		
		if(dbUser == null) 
		{
			dbUser = new LoginInfo();
			dbUser.setUserID("dbuser");
			dbUser.setUserName("dbuser");
			dbUser.setPassWord("dbuser");
			dbUser.setUserType("ROLE_DB");
			logger.info(String.format("Unable to find user with user name: %s", username));
			//throw new BadCredentialsException("1000");
		}
		
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(dbUser.getUserType()));

        return new org.springframework.security.core.userdetails.User(dbUser.getUserName(), dbUser.getPassWord(), grantedAuthorities);
    }
}
