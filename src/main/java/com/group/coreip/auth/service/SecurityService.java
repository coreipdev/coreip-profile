package com.group.coreip.auth.service;

public interface SecurityService {

	public String findLoggedInUsername();
	
	public void login(String username, String password);
	
	public boolean isUserAuthenticated();
}
