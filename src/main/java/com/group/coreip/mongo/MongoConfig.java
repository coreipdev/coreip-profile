package com.group.coreip.mongo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration  //Configuration class
//@ConfigurationProperties(prefix = "coreip.mongodb")
@ConfigurationProperties
public class MongoConfig extends AbstractMongoConfig {

	/**      
     * Implementation of the MongoTemplate factory method      
     * @Bean gives a name (primaryMongoTemplate) to the created MongoTemplate instance      
     * @Primary declares that if MongoTemplate is autowired without providing a specific name, 
     * this is the instance which will be mapped by         default      
     */    
     @Override    
     public @Bean(name = "mongoTemplate") MongoTemplate getMongoTemplate() throws Exception {        
         return new MongoTemplate(mongoDbFactory());    
     }

}
