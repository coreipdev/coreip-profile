package com.group.coreip.mongo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClient;

public abstract class AbstractMongoConfig {

	//Mongo DB Properties
	@Value("${coreip.mongodb.host}")
    private String host;
    
	@Value("${coreip.mongodb.port}")
    private int port;     
    
	@Value("${coreip.mongodb.database}")
    private String database;
    
    //Setter methods go here..     
    
    /*      
     * Method that creates MongoDbFactory     
     * Common to both of the MongoDb connections     
     */    
    public MongoDbFactory mongoDbFactory() throws Exception {        
        return new SimpleMongoDbFactory(new MongoClient(host, port), database);    
    }      
    /*     
     * Factory method to create the MongoTemplate     
     */    
    abstract public MongoTemplate getMongoTemplate() throws Exception;
}
