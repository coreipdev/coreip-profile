package com.group.coreip.services;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.stereotype.Service;

import com.group.coreip.logging.LogValues;
import com.group.coreip.logging.Logger;
import com.group.coreip.models.Conference;
import com.group.coreip.models.User;
import com.group.coreip.postgresConfig.PostgresConnection;
import com.group.coreip.utils.CacheData;
import com.group.coreip.utils.Utility;
import com.itextpdf.xmp.impl.Base64;

@Service
public class UcxInterfaceServiceImpl implements UcxInterfaceService {

	@Override
	public boolean connectConference(Conference conference, String participant) throws Exception {
		String urlPart = "my/conference/" + conference.getConfRoom() + "/invite&" + participant;
		String urlTag = "rest";
		connectUcx(conference.getConfOwner(), conference.getOwnerPwd(), urlPart, "GET", urlTag, null, null);
		return true;
	}

	@Override
	public List<String> fetchConferences(String username, String password, String userID) throws Exception {
		String urlPart = "my/conferences";
		// String urlParam = userID;
		String urlParam = null;
		String urlTag = "rest";
		String xmlData = connectUcx(username, password, urlPart, "GET", urlTag, urlParam, null);

		List<String> conferenceRooms = new LinkedList();

		String[] conferences = xmlData.split("</conference>");
		for (int idx = 0; idx < conferences.length - 1; idx++) {
			String enable = conferences[idx].split("<enabled>")[1].split("</enabled>")[0];
			if (enable.equals("true")) {
				String confRooms = conferences[idx].split("<name>")[1].split("</name>")[0];
				System.out.println("Conf Room : " + confRooms);
				conferenceRooms.add(confRooms);
			}
		}
		return conferenceRooms;
	}

	private String connectUcx(String ucxUsername, String ucxPassword, String urlPart, String reqType, String urlTag,
			String urlParam, String body) throws KeyManagementException, NoSuchAlgorithmException, IOException {

		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		String ucxServerIp = CacheData.getConstantData("ucxServerIP");;
		String servers[] = ucxServerIp.split(",");

		for (int idx = 0; idx < servers.length; idx++) {
			String urlString = "https://" + ucxUsername + ":" + ucxPassword + "@" + servers[idx] + "/sipxconfig/"
					+ urlTag + "/" + urlPart + "/";

			if (null != urlParam) {
				urlString = urlString + urlParam;
			}

			Logger.sysLog(LogValues.error, Utility.class.getName(), urlString);
			URL url = new URL(urlString);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			con.setRequestMethod(reqType);
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setConnectTimeout(60000); // 60 secs
			con.setReadTimeout(60000); // 60 secs
			con.setRequestProperty("Content-Type", "application/json");

			con.setRequestProperty("Authorization", "Basic " + getAuthantication(ucxUsername, ucxPassword));
			if (null != body) {
				Logger.sysLog(LogValues.info, Utility.class.getName(), "Body: " + body);
				OutputStream output = new BufferedOutputStream(con.getOutputStream());
				output.write(body.getBytes());
				output.flush();
			}

			int responseCode = 0;
			try {
				responseCode = con.getResponseCode();
				if (responseCode == 503) {
					return null;
				}
			} catch (SSLHandshakeException e) {
				continue;

			}
			Logger.sysLog(LogValues.info, Utility.class.getName(), reqType + " Request : " + url);
			Logger.sysLog(LogValues.info, Utility.class.getName(), "Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			Logger.sysLog(LogValues.info, Utility.class.getName(), "Got Response: \n" + response);

			return response.toString();
		}
		return null;
	}

	public String getAuthantication(String username, String password) {
		String auth = new String(Base64.encode(username + ":" + password));
		return auth;
	}

	@Override
	public void getUcxUsersInfo() throws Exception {

		String urlPart = "users";
		String reqType = "GET";
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");

		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, null, null);
	}

	@Override
	public ArrayList<User> getUcxUsers() throws Exception {
		String urlPart = "user";
		String reqType = "GET";
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");
		
		ArrayList<User> usersList = null;

		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, null, null);
		if (response != null && response.contains("<totalResults>")) {

			String resultCount = response.split("<totalResults>")[1].split("</totalResults>")[0];
			int userCount = Integer.parseInt(resultCount);
			Logger.sysLog(LogValues.info, this.getClass().getName(), "Number of Ucx Users : " + userCount);
			if (userCount != 0) {
				String users = response.split("<users>")[1].split("</users>")[0];
				usersList = new ArrayList<User> ();
				for (int userIdx = 1; userIdx <= userCount; userIdx++) {
					String userInfo = users.split("<user>")[userIdx].split("</user>")[0];

					String userName = userInfo.split("<userName>")[1].split("</userName>")[0];
					String id = userInfo.split("<id>")[1].split("</id>")[0];
					String firstName = userInfo.split("<firstName>")[1].split("</firstName>")[0];
					String pin = userInfo.split("<pin>")[1].split("</pin>")[0];
					//String firstName = "";
					//String pin = "";
					String sipPassword = userInfo.split("<sipPassword>")[1].split("</sipPassword>")[0];
					User user = new User();
					user.setIpVersion("IPV4");
					user.setExtensionNo(userName);
					user.setFirstName(firstName);
					user.setSipxId(id);
					user.setPin(pin);
					user.setSipPassword(sipPassword);
					user.setIsRegistered("Unregistered");
					getUcxUserPermissions(id, user);
					usersList.add(user);
				}
			} else {
				Logger.sysLog(LogValues.info, this.getClass().getName(), "No users present in UCX");
			}

		}
		return usersList;
	}

	public void getUcxUserPermissions(String id, User user) throws Exception {
		String urlPart = "user-permission";
		String reqType = "GET";
		String urlParam = id;
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");

		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, urlParam, null);
		String[] permissions = response.split("<setting>");
		for (int idx = 1; idx < permissions.length - 1; idx++) {
			String permission = permissions[idx].split("</setting>")[0];
			String permissionName = permission.split("<name>")[1].split("</name>")[0];
			boolean permissionValue;
			if (permission.contains("DISABLE")) {
				permissionValue = false;
			} else {
				permissionValue = true;
			}
			if (permissionName.equals("900Dialing")) {
				user.setDialing900(permissionValue);
			} else if (permissionName.equals("InternationalDialing")) {
				user.setInternationalDialing(permissionValue);
			} else if (permissionName.equals("LocalDialing")) {
				user.setLocalDialing(permissionValue);
			} else if (permissionName.equals("LongDistanceDialing")) {
				user.setLongDistanceDialing(permissionValue);
			} else if (permissionName.equals("RecordSystemPrompts")) {
				user.setRecordSystemPrompts(permissionValue);
			} else if (permissionName.equals("TollFree")) {
				user.setTollFree(permissionValue);
			} else if (permissionName.equals("Voicemail")) {
				user.setVoiceMail(permissionValue);
			} else if (permissionName.equals("music-on-hold")) {
				user.setConfigureMusiconHold(permissionValue);
			} else if (permissionName.equals("personal-auto-attendant")) {
				user.setConfigurePersonalAutoAttendant(permissionValue);
			} else if (permissionName.equals("subscribe-to-presence")) {
				user.setSubscribetoPresence(permissionValue);
			} else if (permissionName.equals("superadmin")) {
				user.setSuperadminAccess(permissionValue);
			} else if (permissionName.equals("tui-change-pin")) {
				user.setChangePINfromIVR(permissionValue);
			}
		}
	}

	@Override
	public void getUcxUserIPV4Info(String userID) throws Exception {

		String urlPart = "user";
		String reqType = "GET";
		String urlParam = userID;
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");

		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, urlParam, null);
	}

	@Override
	public void delUcxUserInfo(String userID) throws Exception {

		String urlPart = "user";
		String reqType = "DELETE";
		String urlParam = userID;
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");

		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, urlParam, null);
	}

	public String setCallerID(User userInfo) throws Exception {

		Statement stmt = null;
		ResultSet rs, rs1, rs2 = null;
		String ret = null;

		if (!userInfo.getCallerId().isEmpty()) {
			Connection conn = PostgresConnection.getPostgresConnection("SIPXCONFIG", "postgres", "12345678");
			if (null != conn) {
				stmt = conn.createStatement();
				stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(
						"Select value_storage_id  from users where user_name = '" + userInfo.getExtensionNo() + "'");
				// System.out.println(rs.isClosed());
				// while (rs.next()) {
				if (!rs.isFirst()) {
					rs.next();
					// System.out.println("ResultSet Size " +rs.getFetchSize());
					String valueStoredID = rs.getString("value_storage_id");
					System.out.println(valueStoredID);
					rs1 = stmt.executeQuery(
							"select * from setting_value where path = 'caller-alias/external-number' and value_storage_id = "
									+ valueStoredID);
					if (!rs1.first()) {
						rs1.next();
						stmt.executeUpdate(
								"insert into setting_value (value_storage_id,value, path) values ('" + valueStoredID
										+ "','" + userInfo.getCallerId() + "', 'caller-alias/external-number')");
						ret = "****Caller ID INSERTED****";
					} else {
						stmt.executeUpdate("update setting_value set value ='" + userInfo.getCallerId()
								+ "' where value_storage_id = " + valueStoredID
								+ "and path ='caller-alias/external-number'");
						ret = "****Caller ID UPDATED****";
					}
				}
			}
		}
		return ret;
	}

	public String prepareUserGroup(User userInfo) throws Exception {
		String grpIDName = ""; // "{\"id\": \"\",\"name\": \"ankit\"},{\"id\":
								// \"\",\"name\": \"asha\"}";
		if (null == userInfo.getGroups()) {
			return "";
		}
		String[] grps = userInfo.getGroups().split(",");
		// System.out.println("LENGTH: " + grps.length);
		// System.out.println(grps[0] + " " + grps[1]);
		for (int idx = 0; idx < grps.length; idx++) {
			String sampleGroup = "{\"group\" : {\"id\": \"_ID\",\"name\": \"_NAME\"}}";
			if (!grps[idx].isEmpty() && grps[idx] != null && grps[idx] != "") {
				// sampleGroup = sampleGroup.replaceFirst("_ID",
				// getIDForGroup(grps[idx]));
				sampleGroup = sampleGroup.replaceFirst("_NAME", grps[idx]);
				System.out.println(sampleGroup);
				grpIDName = grpIDName.concat(sampleGroup);
				if (idx < (grps.length - 1)) {
					grpIDName = grpIDName.concat(",");
				}
			} else {
				grpIDName = "{}";
				break;
			}
		}
		System.out.println("grpNAme" + grpIDName);
		return ",\"groups\": [" + grpIDName + "]";
	}

	@Override
	public String createUcxUserInfo(User user) throws Exception {

		String urlPart = "user";
		String reqType = "PUT";
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");
		String body = "{\"user\": {\"userName\": \"" + user.getExtensionNo() + "\",\"pin\": \"" + user.getPin()
				+ "\",\"sipPassword\": \"" + user.getSipPassword() + "\",\"lastName\":\"" + user.getLastName()
				+ "\",\"firstName\":\"" + user.getFirstName() + "\",\"emailAddress\": \"ab@coreip\""
				+ prepareUserGroup(user) + ",\"aliases\": [{}]}}";
		Logger.sysLog(LogValues.info, this.getClass().getName(),
				"******************** Adding User To UCX ********************\n Body : " + body);

		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, null, body);
		Logger.sysLog(LogValues.info, this.getClass().getName(), "Response : " + response);

		if (response.contains("SUCCESS_CREATED")) {
			String id = response.split("<id>")[1].split("</id>")[0];
			user.setSipxId(id);
			modifyUcxUserPermissions(user);
			String r = setCallerID(user);
			System.out.println(r);

			return id;
		}

		return "Failed";

	}

	@Override
	public String setCallerForwarding(String userName, String password, String forwardingNo) {

		String urlPart = "my";
		String urlParam = "forward";
		String reqType = "PUT";
		String urlTag = "rest";

		String body = "{\"call-sequence\": {\"rings\":[{\"expiration\": 30,\"type\": \"At the same time\","
				+ "\"enabled\": true,\"number\": \"" + forwardingNo
				+ "\"}],\"expiration\": 1,\"withVoicemail\": false}}";

		Logger.sysLog(LogValues.info, this.getClass().getName(),
				"******************** Setting Forwarding ********************\n Body : " + body);

		String response = "";
		try {
			response = connectUcx(userName, password, urlPart, reqType, urlTag, urlParam, body);
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Logger.sysLog(LogValues.info, this.getClass().getName(), "Response : " + response);

		return "Success";

	}

	@Override
	public boolean setPersonalAttendant(String userName, String password) {

		String urlPart = "my";
		String urlParam = "voicemail/attendant";
		String reqType = "PUT";
		String urlTag = "rest";

		String body = "{ \"personalAttendantPermission\":" + CacheData.getConstantData("personalAttendantPermission")
				+ ",\"forwardDeleteVM\":" + CacheData.getConstantData("forwardDeleteVM") + ",\"depositVM\":"
				+ CacheData.getConstantData("depositVM") + ",\"playVMDefaultOptions\":"
				+ CacheData.getConstantData("playVMDefaultOptions") + ",\"overrideLanguage\":"
				+ CacheData.getConstantData("overrideLanguage") + ",\"operator\":\""
				+ CacheData.getConstantData("vmOperator") + "\"" + ",\"menu\":{\"2\":\""
				+ CacheData.getConstantData("callbackCode") + userName + "\"}" + ",\"language\":" + null + "}";

		Logger.sysLog(LogValues.info, this.getClass().getName(),
				"********** Setting Personal Attendant ***********\n Body : " + body);

		String response = "";
		try {
			response = connectUcx(userName, password, urlPart, reqType, urlTag, urlParam, body);
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Logger.sysLog(LogValues.info, this.getClass().getName(), "Response : " + response);

		return true;

	}

	@Override
	public void viewUcxPermissions() throws Exception {
		String urlPart = "permission";
		String reqType = "GET";
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");

		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, null, null);
		// Convert XML to list of permission
		// UcxPermission obj = Utility.convertXmltoObject(dummy,
		// UcxPermission.class);
	}

	@Override
	public void viewUcxUserPermissions(String userID) throws Exception {

		String urlPart = "user-permission";
		String reqType = "GET";
		String urlParam = userID;
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");

		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, urlParam, null);
	}

	@Override
	public String modifyUcxUserPermissions(User userInfo) {

		if (userInfo != null) {
			String sipXId = userInfo.getSipxId();
			for (int idx = 0; idx < 16; idx++) {
				String permissionName = "";
				String permissionValue = "" ;

				switch (idx) {
				case 0:
					permissionName = "900Dialing";
					permissionValue = (userInfo.isDialing900() == true ? "ENABLE" : "DISABLE");
					break;
				case 1:
					permissionName = "dbauth-only";
					permissionValue = (userInfo.isLocalAuthentication() == true ? "ENABLE" : "DISABLE");
					break;
				case 2:
					permissionName = "tui-change-pin";
					permissionValue = (userInfo.isChangePINfromIVR() == true ? "ENABLE" : "DISABLE");
					break;
				case 3:
					permissionName = "personal-auto-attendant";
					permissionValue = (userInfo.isConfigurePersonalAutoAttendant() == true ? "ENABLE" : "DISABLE");
					break;
				case 4:
					permissionName = "music-on-hold";
					permissionValue = (userInfo.isConfigureMusiconHold() == true ? "ENABLE" : "DISABLE");
					break;
				case 5:
					permissionName = "group-music-on-hold";
					permissionValue = (userInfo.isConfigureGroupMusiconHold() == true ? "ENABLE" : "DISABLE");
					break;
				case 6:
					permissionName = "subscribe-to-presence";
					permissionValue = (userInfo.isSubscribetoPresence() == true ? "ENABLE" : "DISABLE");
					break;
				case 7:
					permissionName = "superadmin";
					permissionValue = (userInfo.isSuperadminAccess() == true ? "ENABLE" : "DISABLE");
					break;
				case 8:
					permissionName = "AutoAttendant";
					permissionValue = (userInfo.isAttendantDirectory() == true ? "ENABLE" : "DISABLE");
					break;
				case 9:
					permissionName = "InternationalDialing";
					permissionValue = (userInfo.isInternationalDialing() == true ? "ENABLE" : "DISABLE");
					break;
				case 10:
					permissionName = "LocalDialing";
					permissionValue = (userInfo.isLocalDialing() == true ? "ENABLE" : "DISABLE");
					break;
				case 11:
					permissionName = "LongDistanceDialing";
					permissionValue = (userInfo.isLongDistanceDialing() == true ? "ENABLE" : "DISABLE");
					break;
				case 12:
					permissionName = "Mobile";
					permissionValue = (userInfo.isMobileDialing() == true ? "ENABLE" : "DISABLE");
					break;
				case 13:
					permissionName = "TollFree";
					permissionValue = (userInfo.isTollFree() == true ? "ENABLE" : "DISABLE");
					break;
				case 14:
					permissionName = "Voicemail";
					permissionValue = (userInfo.isVoiceMail() == true ? "ENABLE" : "DISABLE");
					break;
				case 15:
					permissionName = "RecordSystemPrompts";
					permissionValue = (userInfo.isRecordSystemPrompts() == true ? "ENABLE" : "DISABLE");
					break;

				}

				try {
					modifyUcxUserPermission(sipXId, permissionName, permissionValue);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	@Override
	public String modifyUcxUserPermission(String sipXId, String permissionName, String permissionValue)
			throws Exception {

		String urlPart = "user-permission";
		String reqType = "PUT";
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");

		String content = "{\"name\": \"_NAME\",\"value\": \"_FLAG\"}";

		content = content.replaceFirst("_NAME", permissionName);
		content = content.replace("_FLAG", permissionValue);

		String body = "{\"user\": {\"id\": \"" + sipXId + "\",\"permissions\": {\"setting\":" + content + "}}}";
		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, sipXId, body);

		return response;
	}

	@Override
	public String updateUcxUserInfo(User userInfo) throws Exception {

		String urlPart = "user";
		String reqType = "PUT";
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");
		String body = "{\"user\": {\"userName\": \"" + userInfo.getExtensionNo() + "\",\"pin\": \"" + userInfo.getPin()
				+ "\",\"sipPassword\": \"" + userInfo.getSipPassword() + "\",\"lastName\":\"" + userInfo.getLastName()
				+ "\",\"firstName\":\"" + userInfo.getFirstName() + "\",\"emailAddress\": \"ab@coreip\""
				+ prepareUserGroup(userInfo) + ",\"aliases\": [{}]}}";
		Logger.sysLog(LogValues.info, this.getClass().getName(),
				"******************** Updating User To UCX ********************\n Body : " + body);
		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, userInfo.getSipxId(),
				body);
		Logger.sysLog(LogValues.info, this.getClass().getName(), "Response : " + response);
		String reponseString = response.toString();
		if (reponseString.contains("SUCCESS_UPDATED")) {
			String id = reponseString.split("<id>")[1].split("</id>")[0];
			userInfo.setSipxId(id);
			modifyUcxUserPermissions(userInfo);
			String r = setCallerID(userInfo);
			System.out.println(r);

			return id;
		}
		return response.toString();

	}

	@Override
	public HashMap<String, String> getUcxGroups() throws Exception {

		String urlPart = "user-group";
		String reqType = "GET";
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");

		HashMap<String, String> ucxGroups = new HashMap<String, String>();
		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, null, null);
		String reponseString = response.toString();
		if (reponseString.contains("<totalResults>")) {

			String resultCount = reponseString.split("<totalResults>")[1].split("</totalResults>")[0];
			int grpCount = Integer.parseInt(resultCount);
			String[] groups = reponseString.split("</group>");
			for (int idx = 0; idx < grpCount; idx++) {
				String grp = groups[idx].split("<group>")[1];
				String grpId = grp.split("<id>")[1].split("</id>")[0];
				String grpName = grp.split("<name>")[1].split("</name>")[0];
				System.out.println("Grp Name : " + grpName + " Grp ID: " + grpId);
				ucxGroups.put(grpName, grpId);
			}

		}
		return ucxGroups;

		// <user-group> <metadata> <totalResults>33</totalResults>
		// <currentPage>1</currentPage> <totalPages>1</totalPages>
		// <resultsPerPage>33</resultsPerPage> </metadata> <groups> <group>
		// <id>4</id> <name>administrators</name> <description>Users with
		// superadmin privileges</description> </group> <group> <id>15</id>
		// <name>RND</name> <branch> <id>1</id> <name>Abc</name> <address>
		// <id>1</id> <city>Dun</city> </address> </branch> </group> <group>
		// <id>18</id> <name>iThum</name> </group> <group> <id>19</id>
		// <name>A&amp;B</name> </group> <group> <id>21</id> <name>asha</name>
		// </group> <group> <id>22</id> <name>Ankit</name>
		// <description>Testing</description> </group> <group> <id>23</id>
		// <name>null</name> <description>null</description> </group> <group>
		// <id>24</id> <name>CoreIp</name> <description>CoreIp</description>
		// </group> <group> <id>25</id> <name>ABC</name>
		// <description>ABC</description> </group> <group> <id>26</id>
		// <name>Core</name> <description>Core</description> </group> <group>
		// <id>27</id> <name>B1</name> <description>B1</description> </group>
		// <group> <id>28</id> <name>B2</name> <description>B2</description>
		// </group> <group> <id>29</id> <name>C1</name>
		// <description>C1</description> </group> <group> <id>30</id>
		// <name>C2</name> <description>C2</description> </group> <group>
		// <id>31</id> <name>D1</name> <description>D1</description> </group>
		// <group> <id>32</id> <name>D2</name> <description>D2</description>
		// </group> <group> <id>33</id> <name>ithum</name>
		// <description>ithum</description> </group> <group> <id>34</id>
		// <name>G1</name> <description>G1</description> </group> <group>
		// <id>35</id> <name>G2</name> <description>G2</description> </group>
		// <group> <id>36</id> <name>H1</name> <description>H1</description>
		// </group> <group> <id>37</id> <name>H2</name>
		// <description>H2</description> </group> <group> <id>38</id>
		// <name>Q1</name> <description>Q1</description> </group> <group>
		// <id>39</id> <name>Q2</name> <description>Q2</description> </group>
		// <group> <id>40</id> <name>ABCQ</name> <description>ABCQ</description>
		// </group> <group> <id>41</id> <name>Z1</name>
		// <description>Z1</description> </group> <group> <id>42</id>
		// <name>Z2</name> <description>Z2</description> </group> <group>
		// <id>43</id> <name>K12</name> <description>K12</description> </group>
		// <group> <id>44</id> <name>L1</name> <description>L1</description>
		// </group> <group> <id>45</id> <name>U1</name>
		// <description>U1</description> </group> <group> <id>46</id>
		// <name>L3</name> <description>L3</description> </group> <group>
		// <id>47</id> <name>i</name> <description>i</description> </group>
		// <group> <id>79</id> <name>A45</name> <description>A45</description>
		// </group> <group> <id>81</id> <name>A6</name>
		// <description>A6</description> </group> </groups></user-group>
	}

	// @Override
	// public void changePermission() throws Exception {
	//
	//
	// String urlPart = "user-permission";
	// String reqType = "PUT";
	// String urlParam = "66";
	// String body = "{\"user\":{\"id\":\"66\",\"permissions\":
	// {\"setting\":{\"name\":\"900Dialing\",\"value\": \"ENABLE\"}}}}";
	// StringBuffer response = connectUcx(urlPart, reqType, urlParam, body);
	//
	//
	// }
	//

	@Override
	public String createUcxGroups(String grpName) throws Exception {

		String urlPart = "user-group";
		String reqType = "PUT";
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");

		String body = "{\"group\": {\"name\": \"" + grpName + "\", \"description\" : \"" + grpName + "\"}}";
		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, null, body);
		// <?xml version="1.0"
		// encoding="UTF-8"?><response><code>SUCCESS_CREATED</code><message>Created</message><data><id>23</id></data></response>
		String reponseString = response.toString();
		if (reponseString.contains("SUCCESS_CREATED")) {
			String grpId = reponseString.split("<id>")[1].split("</id>")[0];
			return grpId;
		}
		return response.toString();

	}

	@Override
	public void viewUcxUserRegistations(String extensionNo) throws Exception {

		String urlPart = "registrations/user/";
		String reqType = "GET";
		String urlTag = "rest";
		String ucxUserName = CacheData.getConstantData("adminUserName");
		String ucxUserPassword = CacheData.getConstantData("adminUserPassword");

		String response = connectUcx(ucxUserName, ucxUserPassword, urlPart, reqType, urlTag, extensionNo, null);
		System.out.println("Response : " + response);
	}

}
