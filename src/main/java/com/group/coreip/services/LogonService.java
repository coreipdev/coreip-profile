package com.group.coreip.services;

import com.group.coreip.models.LoginInfo;

public interface LogonService {

	public LoginInfo fetchLoginInfo(String userName);
	public LoginInfo fetchLoginInfoByResetID(String resetId);
/*	public String getAccountIdByUser(String username);*/
	boolean updateLoginInfo(LoginInfo info);
	boolean resetPasswordInitiate(String userName);
	boolean savePassword(String resetID, String password);
}
