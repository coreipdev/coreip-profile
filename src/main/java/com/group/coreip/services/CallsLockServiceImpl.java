package com.group.coreip.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.group.coreip.models.SipXSettings;
import com.group.coreip.models.User;
import com.group.coreip.repositories.SipXConfigRepository;
import com.group.coreip.repositories.UcxUsersDao;
import com.group.coreip.utils.CacheData;
import com.group.coreip.utils.Utility;

@Service
public class CallsLockServiceImpl implements CallsLockService {

	@Autowired
	private ProfileService profileService;

	@Autowired
	private SipXConfigRepository profileRepository;

	@Autowired
	private UcxInterfaceService ucxInterfaceService;

	@Autowired
	private UcxUsersDao ucxUsersDao;

	@Override
	public String callsLock(String caller, String password, String permission) {
		// TODO Auto-generated method stub

		String response = "User Not Present";
		for (User user : CacheData.cacheUsers) {
			if (user.getPin().equalsIgnoreCase(password)) {
				if (CacheData.getConstantData("lockUseDbOnly").equalsIgnoreCase("enabled")) {
					SipXSettings userSetting = new SipXSettings();
					userSetting.setValue_storage_id(user.getSipxId());
					userSetting.setValue("DISABLE");
					userSetting.setPath("permission/call-handling/" + permission);
					profileRepository.updateSipXSetting(userSetting, "ENABLE");
					response = "Success";
				} else {
					try {
						response = ucxInterfaceService.modifyUcxUserPermission(user.getSipxId(), permission, "DISABLE");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				response = "Invalid Password";
			}
		}
		return response;
	}

	@Override
	public String callsUnlock(String caller, String password, String permission) {
		// TODO Auto-generated method stub
		String response = "User Not Present";

		for (User user : CacheData.cacheUsers) {
			if (user.getPin().equalsIgnoreCase(password)) {
				if (CacheData.getConstantData("lockUseDbOnly").equalsIgnoreCase("enable")) {
					SipXSettings userSetting = new SipXSettings();
					userSetting.setValue_storage_id(user.getSipxId());
					userSetting.setValue("ENABLE");
					userSetting.setPath("permission/call-handling/" + permission);
					profileRepository.updateSipXSetting(userSetting, "ENABLE");
					response = "Success";
				} else {
					try {
						switch (permission) {
						case "AllDialing":
							if (!user.isInternationalDialing() && user.isInitialInternationalDialing()) {
								response = ucxInterfaceService.modifyUcxUserPermission(user.getSipxId(),
										"InternationalDialing", "ENABLE");
							} else {
								System.out.println("International Dialing is already Enabled for " + caller);
							}
							if (!user.isLocalDialing() && user.isInitialLocalDialing()) {
								response = ucxInterfaceService.modifyUcxUserPermission(user.getSipxId(), "LocalDialing",
										"ENABLE");
							} else {
								System.out.println("Local Dialing is already Enabled for " + caller);
							}
							if (!user.isLongDistanceDialing() && user.isInitialLongDistanceDialing()) {
								response = ucxInterfaceService.modifyUcxUserPermission(user.getSipxId(),
										"LongDistanceDialing", "ENABLE");
							} else {
								System.out.println("LongDistance Dialing is already Enabled for " + caller);
							}
							if (!user.isMobileDialing() && user.isInitialMobileDialing()) {
								response = ucxInterfaceService.modifyUcxUserPermission(user.getSipxId(), "Mobile",
										"ENABLE");
							} else {
								System.out.println("Mobile Dialing is already Enabled for " + caller);
							}
							break;
						case "InternationalDialing":
							if (!user.isInternationalDialing() && user.isInitialInternationalDialing()) {
								response = ucxInterfaceService.modifyUcxUserPermission(user.getSipxId(),
										"InternationalDialing", "ENABLE");
							} else {
								System.out.println("International Dialing is already Enabled for " + caller);
							}
							break;
						case "LocalDialing":
							if (!user.isLocalDialing() && user.isInitialLocalDialing()) {
								response = ucxInterfaceService.modifyUcxUserPermission(user.getSipxId(), "LocalDialing",
										"ENABLE");
							} else {
								System.out.println("Local Dialing is already Enabled for " + caller);
							}
							break;
						case "LongDistanceDialing":
							if (!user.isLongDistanceDialing() && user.isInitialLongDistanceDialing()) {
								response = ucxInterfaceService.modifyUcxUserPermission(user.getSipxId(),
										"LongDistanceDialing", "ENABLE");
							} else {
								System.out.println("LongDistance Dialing is already Enabled for " + caller);
							}
							break;
						case "Mobile":
							if (!user.isMobileDialing() && user.isInitialMobileDialing()) {
								response = ucxInterfaceService.modifyUcxUserPermission(user.getSipxId(), "Mobile",
										"ENABLE");
							} else {
								System.out.println("Mobile Dialing is already Enabled for " + caller);
							}
							break;

						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				response = "Invalid Password";
			}
		}
		return response;
	}

	@Override
	public boolean insertUser(User user) {
		user.setInitialInternationalDialing (user.isInitialInternationalDialing());
		user.setInitialMobileDialing (user.isMobileDialing());
		user.setInitialLongDistanceDialing (user.isLongDistanceDialing());
		user.setInitialLocalDialing(user.isLocalDialing());
		CacheData.cacheUsers.add(user);
		return (ucxUsersDao.insertUser(user));
	}
	
	@Override
	public boolean updateUsers() {
		// TODO Auto-generated method stub
		boolean userPresent = false;
		try {
			ArrayList<User> ucxUsersList = ucxInterfaceService.getUcxUsers();
			ArrayList<User> UsersToUpdate = new ArrayList<User>();
			if (ucxUsersList != null) {
				if (CacheData.cacheUsers == null || CacheData.cacheUsers.isEmpty()) {
					for (User user : ucxUsersList) {
						insertUser(user);
					}
					CacheData.cacheUsers = ucxUsersList;
				} else {
					for (User ucxUser : ucxUsersList) {
						for (User localUser : CacheData.cacheUsers) {
							if (ucxUser.getExtensionNo().equalsIgnoreCase(localUser.getExtensionNo())) {
								if (Utility.campareAndUpdate(localUser, ucxUser, User.class)) {
									UsersToUpdate.add(localUser);
									userPresent = true;
									break;
								}
							}
						}
						if (!userPresent) {
							insertUser(ucxUser);
						}
						userPresent = false;
					}
				}
			}
			for (User user : UsersToUpdate) {
				ucxUsersDao.updateUser(user);
			}
			userPresent = false;
			if (CacheData.cacheUsers.size() > ucxUsersList.size()) {
				for (int idx = 0; idx < CacheData.cacheUsers.size(); idx++) {
					User localUser = CacheData.cacheUsers.get(idx);
					for (User ucxUser : ucxUsersList) {
						if (localUser.getExtensionNo().equalsIgnoreCase(ucxUser.getExtensionNo())) {
							userPresent = true;
							break;
						}
					}
					if (!userPresent) {
						CacheData.cacheUsers.remove(idx);
						ucxUsersDao.deleteUser (localUser.getExtensionNo());
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
