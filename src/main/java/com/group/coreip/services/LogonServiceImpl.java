package com.group.coreip.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.group.coreip.models.LoginInfo;
import com.group.coreip.repositories.LoginDao;

@Service
public class LogonServiceImpl implements LogonService {

	@Autowired
	LoginDao loginDao;
	
	@Autowired
	UcxInterfaceService ucxInterfaceService;
	
	@Override
	public boolean resetPasswordInitiate(String username) {
		// TODO Auto-generated method stub
		LoginInfo loginInfo = fetchLoginInfo(username);
		if (null != loginInfo){} else {
			
		}
		return false;
	}
	
	@Override
	public boolean savePassword(String resetID, String password) {
		// TODO Auto-generated method stub
		LoginInfo loginInfo = fetchLoginInfoByResetID(resetID);
		if (null != loginInfo){} else {
			
		}
		return false;
	}
	
	@Override
	public LoginInfo fetchLoginInfo(String userName) {
		// TODO Auto-generated method stub
		LoginInfo info = loginDao.fetchLoginInfo(userName);
		
		return info;
	}
	
	
	/*public String getAccountIdByUser(String username) {
		
		LoginInfo dbUser = fetchLoginInfo(username);
		
		if(dbUser == null) {
			//logger.info(String.format("Unable to find user with user name: %s", username));
			throw new BadCredentialsException("1000");
		}

        return dbUser.getAccountId();
    }
*/	
	public LoginInfo fetchLoginInfoByResetID(String resetID) {
		return loginDao.fetchLoginInfoByResetID(resetID);
	}
	
	public boolean updateLoginInfo(LoginInfo info) {
		return loginDao.updateLoginInfo(info);
	}


}
