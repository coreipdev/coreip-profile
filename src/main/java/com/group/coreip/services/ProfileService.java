package com.group.coreip.services;

import com.group.coreip.models.SipxUserInfo;
import com.group.coreip.models.UserProfile;

public interface ProfileService {

	public UserProfile fetchSipxUserProfilebyMob(String mobileNo);
	
	public UserProfile fetchSipxUserProfilebyUsername(String username);
	
	public SipxUserInfo fetchSipXUserInfo (String username);
}
