package com.group.coreip.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.group.coreip.models.SipxUserInfo;
import com.group.coreip.models.UserProfile;
import com.group.coreip.repositories.SipXConfigRepository;
import com.group.coreip.repositories.UserProfileMongoRepository;

@Service
public class ProfileServiceImpl implements ProfileService {

	@Autowired
	private SipXConfigRepository profileRepository;
	
	@Autowired
	private UserProfileMongoRepository mongoRepository;
	
	@Autowired
	private MongoTemplate template;
	
	@Override
	public UserProfile fetchSipxUserProfilebyMob(String mobileNo) {
		
		UserProfile profile = null;
		
		try {
			
			Query query = new Query(Criteria.where("m_cellPhoneNumber").is(mobileNo));
			profile = template.findOne(query, UserProfile.class, "userProfile");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(profile == null) {
			System.out.println("profile is NULL!!!!");
			profile = mongoRepository.getProfileByMobileNumber(mobileNo);
		}
		else {
			System.out.println("profile is valued!!!!");
		}
		
		return profile;
	}
	
	@Override
	public UserProfile fetchSipxUserProfilebyUsername(String username) {
		
		UserProfile profile = null;
		
		try {
			
			Query query = new Query(Criteria.where("m_userName").is(username));
			profile = template.findOne(query, UserProfile.class, "userProfile");
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return profile;
	}

	@Override
	public SipxUserInfo fetchSipXUserInfo(String username) {
		return profileRepository.fetchSipXUserInfo(username);
	}
}
