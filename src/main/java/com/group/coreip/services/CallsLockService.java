package com.group.coreip.services;

import com.group.coreip.models.User;

public interface CallsLockService {

	public String callsLock(String caller, String password, String permission);
	public String callsUnlock(String caller, String password, String permission);
	public boolean updateUsers();
	boolean insertUser(User user);
}
