package com.group.coreip.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.group.coreip.models.Conference;
import com.group.coreip.models.LoginInfo;
import com.group.coreip.models.Registrations;
import com.group.coreip.models.User;

public interface UcxInterfaceService 
{
	boolean connectConference(Conference conference, String participant) throws Exception;

	List<String> fetchConferences(String username, String password, String userID) throws Exception;
	
	void getUcxUsersInfo() throws Exception;
	void getUcxUserIPV4Info(String userID) throws Exception;
	void delUcxUserInfo(String userID) throws Exception;
	void  viewUcxPermissions() throws Exception;
	void viewUcxUserPermissions(String userID) throws Exception;
	HashMap<String, String> getUcxGroups() throws Exception;
	String createUcxGroups(String grpName) throws Exception;
	//void changePermission() throws Exception;
	//String updateUcxUserInfo(UserInfo userInfo) throws Exception;

	void viewUcxUserRegistations(String sipXid) throws Exception;
	String createUcxUserInfo(User user) throws Exception;

	String updateUcxUserInfo(User userInfo) throws Exception;
	String setCallerForwarding(String userName, String password, String forwardingNo);

	boolean setPersonalAttendant(String extensionNo, String pin);

	ArrayList<User> getUcxUsers() throws Exception;

	String modifyUcxUserPermission(String sipXId, String permissionName, String permissionValue) throws Exception;

	String modifyUcxUserPermissions(User userInfo);
}
