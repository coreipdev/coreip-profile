<html>
<title>Welcome</title>
<head>
<link rel="stylesheet" media="all" href="<%=request.getContextPath()%>/static/css/jquery.dynatable.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/static/css/jquery-ui.css">

<script type='text/javascript' src="<%=request.getContextPath()%>/static/js/jquery.min.js"></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/jquery.dynatable.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/jquery-ui.js'></script>
<script type='text/javascript' src="<%=request.getContextPath()%>/static/js/createservice.js"></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/account.js'></script>
</head>

<body>

	
	<jsp:include page="header.jsp"></jsp:include>
	<div class="content">
		<div class="header">
			<h1 class="page-title">Welcome</h1>
			
		</div>
		<ul class="breadcrumb">
			<li class="active">Welcome</li>
		</ul>



		<footer>
			<hr>
			<p>
				&copy; 2016 <a href="#" target="_blank">CoreIP</a>
			</p>
		</footer>

	</div>

</body>
</html>