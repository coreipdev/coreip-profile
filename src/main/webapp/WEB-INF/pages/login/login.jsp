<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>

<c:set var="context" value="${pageContext.request.contextPath}" />
<script>
var ctx = "<%=request.getContextPath()%>";
</script>

<title>CoreIP Login</title>

<link rel="stylesheet" media="all" href="<%=request.getContextPath()%>/static/css/coreipCharging.css" />
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/jquery-1.11.3.min.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/common.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/header.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/login.js'></script>
<!-- <link href="images/favicon.ico" rel="shortcut icon"
	type="image/vnd.microsoft.icon" /> -->

<style>
.errorblock {
	color: #ff0000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
</head>
<jsp:include page="forgotPassword.jsp" />
<body onload='document.f.userName.focus();'>


	<div class="loginpanel">
		<h3 class="loginTitle">CoreIP Login</h3>

		
		<form name='f' action="<c:url value='/login' />" method='POST'>
			<ul class="lgform">
				<li>
					<label>UserName:</label>
					<input type='text' id="userName" name="username" value='' />
				</li>
				<li>
					<label>PassWord:</label>
					<input type='password' id="passWord" name="password" value='' />
				</li>
				<li class="margin10">
					<input name="submit" type="submit" value="Submit" />
				</li>
				<li class="txtLeft">
					<a href="#" id="forgetPass">Forgot Password</a>
				</li>
			</ul>
			<%-- <c:if test="${not empty message}">
			<div class="message">Invalid Username or Password</div>
			</c:if> --%>
			<c:if test="${not empty error}"><div class="message">${error}</div></c:if>
			<c:if test="${not empty message}"><div class="message">${message}</div></c:if>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
	</div>
	
</body>
</html>
