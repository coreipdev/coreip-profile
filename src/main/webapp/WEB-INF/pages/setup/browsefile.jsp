<style type="text/css">
	.popup-window {
		width: 35%;
	}
</style>

<div class="popup-window" id="setupBrowseFilePopup" style="top: 270px;left: 35%;"> <a href="#" class="iconsImg close" id="closeSetupBrowseFilePopup">close</a>
	<div class="customer_detials">
		<div class="transaction">
			<h4 class=" cusDetails cust-title">Browse Setup file to upload</h4>
			<div>
				<form id="setupUploadForm" onsubmit="return false;">
					<label>Browse File : </label>
						<input type="file" name="fileToUpload" value="Browse File" />
						<button>Upload File</button>
				</form>
			</div>
		</div>
	</div>
</div>