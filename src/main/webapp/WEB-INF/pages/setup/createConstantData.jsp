<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<div class="popup-window" id="createConstantDataPopup">
	<a href="#" class="iconsImg close" id="closecreateConstantDataPopup">close</a>
	<form id="constantDataForm" onsubmit="return false;">
		<div class="customer_detials">
			<div class="customer-info">
				<ul class="info-hr">
				</ul>
			</div>
			<div class="pbchat-12 txtCenter">
				<h4 class=" cusDetails cust-title">Setup Constant Data</h4>
			</div>
			<div class="activitWall wd80">
				<span class="failed strong" id="errorMsg"> </span>
				<div class="pbchat filter">
					<div class="pbchat-4">
						<label><b>Service Provider Project</b></label>
						<div id="">
							<input type="text" id="spProject" class="spProject"
								name="spProject">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Service Provider Name</b></label>
						<div id="">
							<input type="text" id="spName" class="spName"
								name="spName">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Service Provider Address</b></label>
						<div id="">
							<input type="text" id="spAddress" class="spAddress"
								name="spAddress">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Service Provider City</b></label>
						<div id="">
							<input type="text" id="spCity" class="spCity"
								name="spCity">
						</div>
					</div>
					
					<div class="pbchat-4">
						<label><b>Service Provider State</b></label>
						<div id="">
							<input type="text" id="spState" class="spState"
								name="spState">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Service Provider Contact No</b></label>
						<div id="">
							<input type="text" id="spContactNo" class="spContactNo"
								name="spContactNo">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Service Provider Email Id</b></label>
						<div id="">
							<input type="text" id="spEmailId" class="spEmailId"
								name="spEmailId">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>PayU Product Info</b></label>
						<div id="">
							<input id="payUProductInfo" class="payUProductInfo"
								name="payUProductInfo" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>PayU Merchant Key</b></label>
						<div id="">
							<input id="payUMerchantKey" class="payUMerchantKey"
								name="payUMerchantKey" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>PayU Merchant Salt</b></label>
						<div id="">
							<input id="payUMerchantSalt" class="payUMerchantSalt"
								name="payUMerchantSalt" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>PayU Response Success Url </b></label>
						<div id="">
							<input id="payUResponseSuccessUrl" class="payUResponseSuccessUrl"
								name="payUResponseSuccessUrl" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>PayU Response Failure Url</b></label>
						<div id="">
							<input id="payUResponseFailureUrl" class="payUResponseFailureUrl"
								name="payUResponseFailureUrl" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>PayU Response Cancel Url</b></label>
						<div id="">
							<input id="payUResponseCancelUrl" class="payUResponseCancelUrl"
								name="payUResponseCancelUrl" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>PayU Request Payment Url</b></label>
						<div id="">
							<input id="payURequestPaymentUrl" class="payURequestPaymentUrl"
								name="payURequestPaymentUrl" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Bill Minimum Due Balance</b></label>
						<div id="">
							<input id="billMinimumDueBalance" class="billMinimumDueBalance"
								name="billMinimumDueBalance" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Bill Late Penalty Amount</b></label>
						<div id="">
							<input id="billLatePenaltyAmount" class="billLatePenaltyAmount"
								name="billLatePenaltyAmount" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Bill Pay Allowed Due Days</b></label>
						<div id="">
							<input id="billPayAllowedDueDays" class="billPayAllowedDueDays"
								name="billPayAllowedDueDays" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Bill Pack Base Pack Name</b></label>
						<div id="">
							<input id="billPackBasePackName" class="billPackBasePackName"
								name="billPackBasePackName" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>App Charging Enabled</b></label>
						<div id="">
							<input id="appChargingEnabled" class="appChargingEnabled"
								name="appChargingEnabled" value="">
				
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>App Sip Server</b></label>
						<div id="">
							<input id="appSipServer" class="appSipServer"
								name="appSipServer" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>App Data Server</b></label>
						<div id="">
							<input id="appDataServer" class="appDataServer"
								name="appDataServer" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>App Sip Bridge Server</b></label>
						<div id="">
							<input id="appSipBridgeServer" class="appSipBridgeServer"
								name="appSipBridgeServer" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>App Operator Id</b></label>
						<div id="">
							<input id="appOperatorId" class="appOperatorId"
								name="appOperatorId" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>App Log Dir</b></label>
						<div id="">
							<input id="appLogDir" class="appLogDir"
								name="appLogDir" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Invoice Start Count</b></label>
						<div id="">
							<input id="lastBillNo" class="lastBillNo"
								name="lastBillNo" value="0001">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Next Billing Date</b></label>
						<div id="">
							<input id="nextBillingDate" class="nextBillingDate"
								name="nextBillingDate" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Last Billing Date</b></label>
						<div id="">
							<input id="lastBillingDate" class="lastBillingDate"
								name="lastBillingDate" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Service Tax %</b></label>
						<div id="">
							<input id="sTax" class="sTax"
								name="sTax" value="14">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>KKC Tax %</b></label>
						<div id="">
							<input id="kkcTax" class="kkcTax"
								name="kkcTax" value="0.5">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Email Id</b></label>
						<div id="">
							<input id="spEmailId" class="spEmailId"
								name="spEmailId" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Pdf Templet Path</b></label>
						<div id="">
							<input id="postpaidBillsPdfsPath" class="postpaidBillsPdfsPath"
								name="postpaidBillsPdfsPath" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Pdf Path</b></label>
						<div id="">
							<input id="pdfTemplatePath" class="pdfTemplatePath"
								name="pdfTemplatePath" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Bank Account No</b></label>
						<div id="">
							<input id="spAccountNo" class="spAccountNo"
								name="spAccountNo" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>CIN No</b></label>
						<div id="">
							<input id="spCinNo" class="spCinNo"
								name="spCinNo" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>IFCS Code</b></label>
						<div id="">
							<input id="spIfcsCode" class="spIfcsCode"
								name="spIfcsCode" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Regd Off Address</b></label>
						<div id="">
							<input id="spRegdOfc" class="spRegdOfc"
								name="spRegdOfc" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>SP Service Tax No</b></label>
						<div id="">
							<input id="spServiceTaxNo" class="spServiceTaxNo"
								name="spServiceTaxNo" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Receipt No</b></label>
						<div id="">
							<input id="lastReceiptNo" class="lastReceiptNo"
								name="lastReceiptNo" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Support Email</b></label>
						<div id="">
							<input id="supportEmail" class="supportEmail"
								name="supportEmail" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Support Password</b></label>
						<div id="">
							<input id="supportPassword" class="supportPassword"
								name="supportPassword" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Support TLS Enabled</b></label>
						<div id="">
							<input id="supportTlsEnabled" class="supportTlsEnabled"
								name="supportTlsEnabled" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Smtp Host</b></label>
						<div id="">
							<input id="smtpHost" class="smtpHost"
								name="smtpHost" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>Support Smtp Port</b></label>
						<div id="">
							<input id="supportSmtpPort" class="supportSmtpPort"
								name="supportSmtpPort" value="">
						</div>
					</div>
					<div class="pbchat-4">
						<label><b>SBC Tax %</b></label>
						<div id="">
							<input id="sbcTax" class="sbcTax"
								name="sbcTax" value="0.5">
						</div>
						<input type="hidden" name="constantDataUpdate" id="constantDataUpdate"
							value="${constantDataUpdate}" />
						<div class="pbchat-12 txtCenter " id="saveUpdateConstantDataBtn">
							<button id="createConstantData">Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</html>



