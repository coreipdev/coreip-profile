<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
<meta charset="utf-8">
<title>Settings</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<%-- <link href="${pageContext.request.contextPath}/resources/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" /> --%>
<link rel="stylesheet" media="all" href="<%=request.getContextPath()%>/static/css/coreipCharging.css" />
<link rel="stylesheet" media="all" href="<%=request.getContextPath()%>/static/css/uikit.min.css" />
<link rel="stylesheet" media="all" href="<%=request.getContextPath()%>/static/css/jquery.dynatable.css" />
<link rel="stylesheet" media="all" href="<%=request.getContextPath()%>/static/css/jquery-ui.css" />


<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/jquery-1.11.3.min.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/jquery.dynatable.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/jquery-ui.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/uikit.min.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/header.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/common.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/static/js/constant.js'></script>

<%-- <script src="${pageContext.request.contextPath}/resources/script/insurerDashboard.js" type="text/javascript"></script> --%>


</head>


<body>
	
	<div class="overlay"></div>

	<div class="gateway_container fluid">
	<jsp:include page="../header.jsp" />
		<jsp:include page="createConstantData.jsp" />
		<jsp:include page="updateConstantData.jsp" />
		   <jsp:include page="browsefile.jsp" />
		   <jsp:include page="browseTarFile.jsp" />
		   
		<!-- <span class="failed strong" id="errorMsg"></span> -->
		<div class="blur">
			<div class="base">
			
				
				<div class="lastDisplay updateHeader_margin">
					<div class="tableArea">
						<div class="tableContent">
							<div class="tableHighlight">
								<span class="alltran">Settings</span>  
							</div>
							
							<div class="tableBox">
								<jsp:include page="constantDataDetails.jsp" />
								<span id="noRecords" style="display: none; text-aling: center; width: 200%; padding: 10px 20px 10px 10px;"> No Country to display </span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>

