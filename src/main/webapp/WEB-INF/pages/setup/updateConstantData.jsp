<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<div class="popup-window" id="updateConstantDataPopup">
	<a href="#" class="iconsImg close" id="closeupdateConstantDataPopup">close</a>
	<form id="constantDataUpdateForm" onsubmit="return false;">
		<div class="customer_detials">
			<div class="customer-info">
				<ul class="info-hr">
				</ul>
			</div>
			<div class="pbchat-12 txtCenter">
				<h4 class=" cusDetails cust-title">Update Constant Data</h4>
			</div>
			<div class="activitWall wd80">
				<span class="failed strong" id="errorMsg2"> </span>
				<div class="pbchat filter">
					<div class="pbchat-4">
						<input type="hidden" id="updateK" value="" />
					    <input type="hidden" id="updateN" value="" />	
					    <input type="hidden" id="updateT" value="" />
					    <input type="hidden" id="updateD" value="" />
					    
						<div id="">
							<span class="wd5" id="updateName"></span>
						</div>
						</div>
				
					<div class="pbchat-4" id="textBox">
						<div id="">
							<input type="text" id="updateText" class="updateText"/>
						</div>
					</div>
					
					<div class="pbchat-4" id="textBoxPassword">
						<div id="">
							<input type="password" id="updateTextPwd" class="updateTextPwd"/>
						</div>
					</div>
					
					<div class="pbchat-4" id="dropDownList">
						<select name="updateDropDown" id="updateDropDown"></select>
					</div>
					
					<div class="pbchat-4" id="date">
			          <span class="failed strong" id="errorMsg"> </span>
			          <div>
				       <!-- <label>Start Date: </label> -->
				       <input name="startDate" id="startDate"/>
			         </div>
		           </div>
					
					
					<div class="pbchat-4" id="number">
						<div id="">
							<input type="number" id="updateNumber" class="updateNumber"/>
						</div>
					</div>
					
					<div class="pbchat-4" id="email">
						<div id="">
							<input type="email" id="updateEmail" class="updateEmail"/>
						</div>
					</div>
					
					
					<div class="pbchat-4">
						<div class="pbchat-12 txtCenter " id="updateConstantDataBtn">
							<button id="updateConstantDataBtn">Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</html>



