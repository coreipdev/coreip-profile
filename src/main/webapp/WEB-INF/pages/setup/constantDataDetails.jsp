<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
</head>

<ul class="cusTable" id="insurerReportTable">
	<div class="cusHeader">
		<li class="cusRow"> <span class="wd20"> Config </span> 
			<span class="wd20"> Value </span>
			<!-- <span class="wd10" ><a href="#" class = "addConstantData">Add Config</a> </span>
			<span class="wd10" ><span style="color: #1d8acb; cursor: pointer;" class="setupFileUpload">Import</span></span> -->
			<span style="color: #1d8acb; cursor: pointer;"><a href="exportSetupCSV" id = "1">Export</a></span>
			<span style="color: #1d8acb; cursor: pointer;"><a href="createBackup" id = "2">Create Backup</a> </span>
			<span style="color: #1d8acb; cursor: pointer;" id="restoreMongoBackup" id = "2">Restore Data</a> </span>
			<span class="wd10" ><span style="color: #1d8acb; cursor: pointer;" class="version">Version</span>
			
		</li>
	</div>


	<c:forEach var="constantData" items="${constantDataList}">
		<c:set var="statusColor" value="other" />
		<div class="cusBody">
			<li class="cusRow">
				<input type="hidden" value="${constantData['KEY']}" class="key" /> 
				<span style="color: blue" class="wd5">${constantData['NAME']}</span>
				<c:choose>
					<c:when test="${constantData['TYPE'] eq 3}">
				      <span class="wd10">*****</span>
				    </c:when>
					<c:otherwise>
                     <span class="wd10">${constantData['VALUE']}</span>
				   </c:otherwise>
				</c:choose>
				<span class="updateConstantData" align="center"><a href="#"> Update </a></span>
				
			</li>
		</div>
	</c:forEach>

</ul>

</html>