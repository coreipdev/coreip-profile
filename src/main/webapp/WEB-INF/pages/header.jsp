<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set var="isAdmin" value="true" />

<c:set var="context" value="${pageContext.request.contextPath}" />
<script>
var ctx = "<%=request.getContextPath()%>";
</script>
<div class="topHead">
	<div class="header">
		<div class="pbchat bdr">
			<div class="pbchat-6 lRpadd">
				<a href="#"><img src="/sipxbridge/static/images/logo.png" width="198" height="39" alt="" /></a>
			</div>
			<div class="pbchat-2">
				<div class="clientLogo txtRight">

					
				</div>
			</div>
			
			<div class="pbchat-3">
				<c:url value="/logout" var="logoutUrl" />
				<form id="logout" action="${logoutUrl}" method="post" >
				
				  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				</form>
				
					<a><%= new java.util.Date() %></a>&nbsp;&nbsp;|&nbsp;
					<span>${pageContext.request.userPrincipal.name}</span>&nbsp;&nbsp;|
				<c:if test="${pageContext.request.userPrincipal.name != null}">
					<a href="javascript:document.getElementById('logout').submit()" style="margin-left:10px;">Logout</a>
				</c:if>
			</div>
			<br><br><br><br>
		</div>
	</div>
</div>
<div class="nav">
		<ul class="menu">
			<c:choose>
				<c:when test="${isAdmin}">
					<li><a href="#" class="reportType" id="constantdataheader">Settings</a></li>
				</c:when>
			</c:choose>
		</ul>
</div>
