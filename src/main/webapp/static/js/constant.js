$(document)
		.ready(
				function() {

					resetfeilds();
					$("#startDate").datepicker();

					function clearInputs() {
						$("#clearInputs .a :input").each(function() {
							//e.stopImmediatePropagation();
							$(this).val('');
						});
					}
			
					$("#closeupdateConstantDataPopup").click(function() {
						//e.stopImmediatePropagation();
						openAndClose_popup(0, 'updateConstantDataPopup');
						window.location.href = ctx + "/constantDataDetails";
					});
					resetfeilds();

					function resetfeilds() {
						$('#errorMsg').text('');
					} // end of reset feilds
					

					$('.version').on("click", function(e) {
						e.stopImmediatePropagation();
						var url = ctx+"/findVersion";	
						
					jQuery.ajax({
						url : url,
						contentType : "application/json",
						
						success : function(response) {
							// = response.data.ver;
							$ver=null;
							ver =response.value;
							if(ver!=null){
								alert(ver);
							}
						},
						error : function(jqXHR, textStatus, errorThrown) {
							var x = 1;
						}
					});
				});
					
					
					
				
					
					$('#constantDataUpdateForm').on("submit", function(e) {
						e.stopImmediatePropagation();
						var form = $('#constantDataUpdateForm');
						var url = "updateConstantData";	
						var flag = true;
						var updateDropDown = $('#updateDropDown').val();
						var updateText = $('#updateText').val();
					    var updateTextPwd = $('#updateTextPwd').val();
					    var startDate = $('#startDate').val();
					    var updateNumber = $('#updateNumber').val();
					    var updateEmail = $('#updateEmail').val();
					    var type = $('#updateT').val();
					    var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	
					   
						if (updateText == "" &&  updateDropDown == "" && updateTextPwd == "" && startDate == "" && updateNumber == "" && (updateEmail == ""||(reg))|| updateText == null && updateDropDown == null && updateTextPwd == null && startDate == null && updateNumber == null && updateEmail == null) {
							$('#errorMsg2').text('Enter Valid Value');
							flag = false;
						}
						
						
					
						if (type == "1") 
							value = updateText;	
					    else if (type == "2")	
						     value = updateDropDown;
					   else if (type == "3")	
							 value = updateTextPwd;    
					   else if (type == "4")	
							 value = startDate;
					   else if (type == "5")	
							 value = updateNumber;
					   else if (type == "6")	
							 value = updateEmail;
						    
						if(flag){
						var data = '{"KEY" : "'
							+ $('#updateK').val()
							+'","NAME" : "'
							+$('#updateN').val()
							+'" , "VALUE" : "'
							+ value
							+'" , "DEFAULTVAL" : "'
							+$('#updateD').val()
							+'"}';
						openAndClose_popup(0,
						'updateConstantDataPopup');
					jQuery.ajax({
						url : url,
						type : "GET",
						data : {
							'data' : data
						},
						contentType : "application/json",
						
						success : function(response) {
							window.location.href = ctx + "/constantDataDetails";
						},
						error : function(jqXHR, textStatus, errorThrown) {
							var x = 1;
						}
					});
				  }
				});
						
					
					$('.updateConstantData').on("click",
							function(e) {
								e.stopImmediatePropagation();
								var key = $(this).parents("li")
								.find('.key').val();

								jQuery
								.ajax({
									url : "getConstantDataInfo",
									type : "GET",
									data : {
										'Key' : key
									},
									
									contentType : "application/json",

									success : function(response) {
										if (response.status == 200) {
											openAndClose_popup(
													1,
													'updateConstantDataPopup');
											
											
											$('#updateK').val(response.data.key);
											$('#updateN').val(response.data.name);
											$('#updateT').val(response.data.type);
										    $('#updateName').text(response.data.name);
										    $('#updateD').val(response.data.defaultval);
										    
										    
  										    /*if (response.data.key == "transport_pref"){
  										    	var packId1	  = "TCP";
  										    	var packName1 = "TCP";
  										    	var packId2   = "UDP";
  										    	var packName2 = "UDP";
  										    }else{
  										       var packId1   = "Disable";
  										       var packName1 = "Disable";
  										       var packId2   = "Enable";
										       var packName2 = "Enable";
  										    }*/
										    
											
  										    if (response.data.type == 1) {
												
  										    	//textbox	
  										    	$('#updateText').val(response.data.value);
											    $('#textBox').show();											   
											    $('#dropDownList').hide();
											    $('#textBoxPassword').hide();
											    $('#date').hide();
											    $('#number').hide();
											    $('#email').hide();
											
											} else if (response.data.type == 2) {
												
												//dropdown
												var defValues = $("#updateD").val().split(',');
												
												for(var i=0; i < defValues.length ; i++)
												{
													var html = $("#updateDropDown").html();
													var newHtml = html + '<option id="' + defValues[i] + '" value="' + defValues[i] + '">' + defValues[i] + '</option>';
													$("#updateDropDown").html(newHtml);
												}
												
												$('#updateDropDown').val(response.data.value);
												$('#textBox').hide();
												$('#dropDownList').show();
												$('#textBoxPassword').hide();
												$('#date').hide();
												$('#number').hide();
												$('#email').hide();
												
											} else if (response.data.type == 3) {
												
												//xyz password
												$('#updateTextPwd').val(response.data.value);
												$('#textBox').hide();
												$('#dropDownList').hide();
												$('#textBoxPassword').show();
												$('#date').hide();
												$('#number').hide();
												$('#email').hide();
												
                                             } else if (response.data.type == 4) {
												
												//date
                                            	$('#startDate').val(response.data.value);
                                            	$('#date').show();
												$('#textBox').hide();
												$('#dropDownList').hide();
												$('#textBoxPassword').hide();
												$('#number').hide();
												$('#email').hide();
												
												
												
                                             } else if (response.data.type == 5) {
 												
 												//number
                                            	$('#updateNumber').val(response.data.value);
                                            	$('#number').show(); 
                                             	$('#date').hide();
 												$('#textBox').hide();
 												$('#dropDownList').hide();
 												$('#textBoxPassword').hide();
 												$('#email').hide();
 												
 												
                                             } else if (response.data.type == 6) {
  												
  												//email
                                            	$('#updateEmail').val(response.data.value);
                                            	$('#email').show();
                                             	$('#number').hide(); 
                                              	$('#date').hide();
  												$('#textBox').hide();
  												$('#dropDownList').hide();
  												$('#textBoxPassword').hide();
  												
											}
  										    

										}
									}
								})
							});

					
					$('#setupUploadForm').on("submit", function(){
		            	   //e.stopImmediatePropagation();
			              console.log("Submit upload form");
			           $.ajax({
				            url: "uploadSetupCSV",
				            type: "POST",
				            data: new FormData($("#setupUploadForm")[0]),
				            enctype: 'multipart/form-data',
				            processData: false,
				            contentType: false,
				            cache: false,
				            success: function(response) {
					        openAndClose_popup(0, 'setupBrowseFilePopup');
				        },
				        error : function(jqXHR, textStatus, errorThrown) {
					    var x = 1;
				    }
			     });
		    });
					
					$('.setupFileUpload').click(function() {
						//e.stopImmediatePropagation();
						openAndClose_popup(1, 'setupBrowseFilePopup');
					});
					
					$("#closeSetupBrowseFilePopup").click(function() {
						//e.stopImmediatePropagation();
						clearInputs();
						openAndClose_popup(0, 'setupBrowseFilePopup');
					});
			
				}); // end
