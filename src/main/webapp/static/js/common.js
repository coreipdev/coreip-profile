/**
 * 
 */

$(document).ready(function(e) {
      
	$("#closePaymentDetailPopup").click(function() {
		//e.stopImmediatePropagation();
		openAndClose_popup(0, 'paymentDetailsPopup');
	});
	
	$("#closeSettlementDetailPopup").click(function() {
		//e.stopImmediatePropagation();
		 $("#mytable > tbody").html("");
		openAndClose_popup(0, 'paymentDetailsPopup');
	});
	
	
	
	//filter js
	$('.daysFilter').each(function () {
		//e.stopImmediatePropagation();

	    var $this = $(this),
	        numberOfOptions = $(this).children('option').length;

	    $this.addClass('s-hidden');
	    $this.wrap('<div class="select"></div>');

	    $this.after('<div class="styledSelect"></div>');

	    var $styledSelect = $this.next('div.styledSelect');

	    $styledSelect.text($this.children('option').eq(0).text());

	    var $list = $('<ul />', {
	        'class': 'options'
	    }).insertAfter($styledSelect);

	    for (var i = 0; i < numberOfOptions; i++) {
	        $('<li />', {
	            text: $this.children('option').eq(i).text(),
	            rel: $this.children('option').eq(i).val()
	        }).appendTo($list);
	    }
		$list.append('<li class="checkFocus"><div class="startDate fleft" ><input type="text" class="dates start fromdate" placeholder="DDMMYYYY" data-uk-datepicker="{format:"YYYY-MM-DD",pos:"bottom"}"  /></div><div class="endDate fleft"><input type="text" placeholder="DDMMYYYY" class="dates end todate" data-uk-datepicker="{format:"YYYY-MM-DD",pos:"bottom"}" /></div><div class="datesubmit"><input type="button" class="datesubmit" value="GO"></div></li>')
	    var $listItems = $list.children('li');
	    $styledSelect.click(function () {
	        //e.stopPropagation();
	        $('div.styledSelect.active').each(function () {
	        	//e.stopImmediatePropagation();
	            $(this).removeClass('active').next('ul.options').hide();
	        });
	        $(this).toggleClass('active').next('ul.options').toggle();
	    });

	    $listItems.click(function () {
	    	//e.stopImmediatePropagation();
			if($(e.target).hasClass('dates')){e.stopPropagation();}
			else if($(e.target).hasClass('datesubmit')){e.stopPropagation();}
			else{
	        e.stopPropagation();
	       // var text = "-";
	        /*if ($(this).text().toUpperCase() !== "GO") {
	        	text = $(this).text();
	        }*/
	        $styledSelect.text($(this).text()).removeClass('active');
	        $this.val($(this).attr('rel'));
	        $list.hide();
	        $('#dayDateFilterSelection').val("days");
			}
	    });
		$('.datesubmit').click(function(){
			//e.stopImmediatePropagation();
			var start = $('.checkFocus input.start').val();
			var end = $('.checkFocus input.end').val();
			$styledSelect.text(start+"-"+end).removeClass('active');
			$list.hide();
			//$('.uk-dropdown').hide();
			$('#dayDateFilterSelection').val("date");
		});

	    
	    $(document).click(function () {
	    	//e.stopImmediatePropagation();
	        $styledSelect.removeClass('active');
	        $list.hide();
	    });

	});
	/**filter js end */
	
	
});

function showPaymentDetails() {
	openAndClose_popup(1, 'paymentDetailsPopup');
}

function openAndClose_popup(val, popupid){
	
	
	var divH = $('#'+ popupid).height();
	var windH = ($(window).height() -divH)/2;
	var windW = ($(document).width()-$('#'+ popupid).width())/2;
	
	if(val == 1){
		$('.overlay').show().animate({'opacity':1});
		$('.blur').addClass('bgBlur'); 
		$('#'+ popupid).css({'top': -(divH),'left':windW});
		$('#'+ popupid).show().animate({'top':windH});
	}
	else{
		$('.overlay').animate({'opacity':0, 'display':'none'});		
		$('.blur').removeClass('bgBlur');
		console.log('removed')
		$('#' + popupid).animate({'top':-divH,'display':'none'},300,function(){
			//e.stopImmediatePropagation();
			$('#' + popupid).removeAttr('style');	
			$('.overlay').removeAttr('style');	
		})
		
	}
}

function showLoader() {
	$('.overlay').show().animate({'opacity':1});
	$('.fluid').addClass('bgBlur'); 
	$('.loader').show();
}

function closeLoader() {
	$('.overlay').animate({'opacity':0});	
	$('.overlay').removeAttr('style');	
	$('.fluid').removeClass('bgBlur');
	$('.loader').hide();	
}

function calculateDays(startDate, endDate) {
	var oneDay = 24*60*60*1000;    // hours*minutes*seconds*milliseconds
	
	// Now we convert the array to a Date object, which has several helpful methods
	date1 = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
	date2 = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());

	// We use the getTime() method and get the unixtime (in milliseconds, but we want seconds, therefore we divide it through 1000)
	date1_unixtime = parseInt(date1.getTime());
	date2_unixtime = parseInt(date2.getTime());

	// This is the calculated difference in seconds
	var timeDifference = date2_unixtime - date1_unixtime;

	var days = timeDifference / oneDay ;
	
	if (days > 30) {
		return false;
	} else {
		return true;
	}
}

function calculateDaysforWeek(startDate, endDate) {
	var oneDay = 24*60*60*1000;    // hours*minutes*seconds*milliseconds
	
	// Now we convert the array to a Date object, which has several helpful methods
	date1 = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
	date2 = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());

	// We use the getTime() method and get the unixtime (in milliseconds, but we want seconds, therefore we divide it through 1000)
	date1_unixtime = parseInt(date1.getTime());
	date2_unixtime = parseInt(date2.getTime());

	// This is the calculated difference in seconds
	var timeDifference = date2_unixtime - date1_unixtime;

	var days = timeDifference / oneDay ;
	
	if (days > 7) {
		return false;
	} else {
		return true;
	}
}